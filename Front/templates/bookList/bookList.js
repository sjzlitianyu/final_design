const app=getApp();
Page({
    data: {
        likenum:'',
        collectnum:'',
        homeinfodata:[],
        imgdata:'',
        collectimg:'收藏',
        loveimg:"/icon/tabicon/love.png",
    },
    onLoad: function (options) {
        var that=this;
        const eventChannel = this.getOpenerEventChannel()
        eventChannel.on('acceptInfo', function(data) {
            that.setData({homeinfodata:data.data})
            console.log(that.data.homeinfodata)
            wx.request({
              url: `${app.globalData.url}/collect/select`,
              method: 'POST',
              data: {
                openid: app.globalData.openid,
                content:that.data.homeinfodata.content
              },
              header: {
                "Content-Type": "application/x-www-form-urlencoded"
              },
              success:res=>{
                if(res.data===2){
                  that.setData({collectimg:"/icon/tabicon/collected.png"})
                }else{
                  that.setData({collectimg:"/icon/tabicon/collect.png"})
                }
              }
            })
            wx.request({
              url: `${app.globalData.url}/like/select`,
              method: 'POST',
              data: {
                openid: app.globalData.openid,
                content:that.data.homeinfodata.content,
              },
              header: {
                "Content-Type": "application/x-www-form-urlencoded"
              },
              success:res=>{
                if(res.data===2){
                  that.setData({loveimg:"/icon/tabicon/loveed.png"})
                }else{
                  that.setData({loveimg:"/icon/tabicon/love.png"})
                }
              }
            })
            wx.setNavigationBarTitle({
                title: data.data.class,
              })
        })
        eventChannel.on('acceptImg', function(data) {
            that.setData({imgdata:data.data})
        })
    },
    collectIt(e){
        var time=Date.parse(new Date())/1000;
        if(this.data.collectimg==="/icon/tabicon/collect.png"){
            wx.request({
                url: `${app.globalData.url}/collect/insert`,
                method: 'POST',
                data: {
                  openid: app.globalData.openid,
                  time:time,
                  content:this.data.homeinfodata.content,
                  class:this.data.homeinfodata.class,
                  name:this.data.homeinfodata.name,
                  releasetime:this.data.homeinfodata.releasetime
                },
                header: {
                  "Content-Type": "application/x-www-form-urlencoded"
                },
                success:res=>{
                  this.setData({collectimg:"/icon/tabicon/collected.png"})
                  console.log('yes');
                }
              })
        }else{
            wx.request({
                url: `${app.globalData.url}/collect/delete`,
                method: 'POST',
                data: {
                  openid: app.globalData.openid,
                  content:this.data.homeinfodata.content
                },
                header: {
                  "Content-Type": "application/x-www-form-urlencoded"
                },
              })
            this.setData({collectimg:"/icon/tabicon/collect.png"})
        }
    },

})
