const app = getApp();
var tag = 0;
let score = 0;
const innerAudioContext = wx.createInnerAudioContext();
innerAudioContext.src = "https://www.finaldesign.site:5050/image/click2.mp3";
innerAudioContext.loop = true;
Page({
  data:{
    arr:[],//题库
    tags:0,//题目标识符
    newArr: [], //随机题目数组
    optionsArr:[],//选项数组
    chooseContent:[], //记录所选内容
    score:0,
    time:120//计时器时间
  },
  timeClick:function () {
    setInterval(function() {
      this.data.time = this.data.time - 1;
      this.setData({
        time:this.data.time
      })
    },1000)
  },
  beforQuestion:function () {
    tag = tag - 1;
    if(tag < 0){
      wx.showToast({
        title:'这已经是第一道题了！'
      })
    }else{
      this.setData({
        tags:tag
      })
    }
  },
  submit:function () {
    tag = tag + 1;
    if(tag > 4){
      wx.showToast({
        title:`您已经完成本次测验！`
      })
      wx.navigateTo({
        url: `../scorePage?score=${this.data.score}`
      })
    }else{
      this.setData({
        tags:tag,
        idx:4
      })
    }
  },
  chooseAnswer:function (res) {
    let index = res.currentTarget.dataset.index;
    let num = this.data.tags;
    let trueContent = this.data.newArr[num].answer;
    let chooseOption = this.data.optionsArr[num][index];
    if(trueContent === chooseOption){
      score += 20;
    }
    this.setData({
      chooseContent:[...this.data.chooseContent,chooseOption],
      score:parseInt(score),
      idx:res.currentTarget.dataset.index
    })
  },
  onLoad:function (options) {
    innerAudioContext.play();
    setInterval(()=>{
      let timeData = this.data.time;
      timeData --;
      this.setData({
        time:timeData
      })
    },1000)
    switch(options.type){
      case 'revolution':
        this.setData({
          score: 0
        })
        break;
      case 'science':
        this.setData({
          score: 0
        })
        break;
      case 'medicine':
        this.setData({
          score: 0
        })
        break;
    };
    wx.request({
      url: 'https://www.finaldesign.site:5050/question',
      success:res => {
        this.setData({
          arr:res.data
        });
        console.log(res.data)
        var random_num = parseInt(Math.random() * res.data.length);
        var n1 =(random_num + res.data.length)%res.data.length;
        var n2 =(random_num + res.data.length + 1)%res.data.length;
        var n3 =(random_num + res.data.length + 2)%res.data.length;
        var n4 =(random_num + res.data.length + 3)%res.data.length;
        var n5 =(random_num + res.data.length + 4)%res.data.length;
        this.setData({
          newArr:[this.data.arr[n1],this.data.arr[n2],this.data.arr[n3],this.data.arr[n4],this.data.arr[n5]]
        });
        console.log(this.data.newArr);
        for(var i = 0; i < this.data.newArr.length;i++){
          var arr_tmp = [];
          arr_tmp.push(this.data.newArr[i].option_A);
          arr_tmp.push(this.data.newArr[i].option_B);
          arr_tmp.push(this.data.newArr[i].option_C);
          arr_tmp.push(this.data.newArr[i].option_D);
          this.setData({
            optionsArr:[...this.data.optionsArr,arr_tmp]
          })
        }
      }
    });
  },
  onHide:function () {
    innerAudioContext.stop();
  },
  onUnload:function () {
    innerAudioContext.stop();
  }
})