// pages/answer_question/scorePage.js
const app=getApp()
Page({
  data: {},
  goToQuestion:function(options){
    wx.navigateTo({
      url: '../question/question',
    })
  },
  onLoad: function (options) {
    var time = Date.parse(new Date()) / 1000;
    this.setData({
      score:options.score
    })
    wx.request({
      url: 'https://www.finaldesign.site:5050/question/insertmark',
      method:"POST",
      data:{
        openid: app.globalData.openid,

        mark:this.data.score,
        time:time
      }
    })
  }
})