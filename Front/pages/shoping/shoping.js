// pages/shoping/shoping.js
const {host,shopingUrl,updateUrl,deleteUrl}=require('../../common/config')
Page({

    data: {
        // isActive:false,//不显示删除按钮的
        startX:'',//点击的x位置
        selected:false,//列表的按钮 默认选中状态
        list:[],//购物列表数据
        selectAllStatus:false,//全选按钮
        selectButton:false,//结算是否高亮
        num:0,//统计选中的个数 
        totalPrice:'00.00',
        info:'',//存储你选中商品的id 
    },
    //3.点击购物车列表 选中数据--------------------------------
    selected(e){
       //选中的变量 item.selected 给list数组对象 添加一个属性控制
       //思路：1.获取当前操作的元素下标  2.获取数据list容器 3.同步数据
       const list=this.data.list//获取操作的数组
       let num=this.data.num//
       const index=e.currentTarget.dataset.index;//获取下标
       //点击 修改对应的元素的 selected状态 true/false 
       list[index].selected=!list[index].selected

        //如果选中 当前的元素 num++ 
        if(list[index].selected){
            num++
        }else{
            num--;
        }
        console.log(num);
        
        if(num == list.length){//全选 选中
            this.setData({
                selectAllStatus:true
            })
        }else{
            this.setData({
                selectAllStatus:false
            })
        }

       //同步数据
       this.setData({
           list:list,
           num:num
       })
       //计算价格
       this.getTotalPrice();
    },
    //4.全选按钮--点击操作------------------------------------
    selectAll(){ //全选就是根据全选状态 selectAllStatus 去改变每个商品的 selected
        //思路：1.点击全选按钮--取反状态  2.遍历list容器对selected属性同步设置状态 3.同步
        let selectAllStatus = !this.data.selectAllStatus; // 是否全选状态
        //获取容器list 修改里面的对应的选中状态
        let list = this.data.list;
        let num=this.data.num;
        //遍历list容器 同步修改状态  全选选中 列表都选中，全选取消都是取消 状态一样      
        for (let i = 0; i < list.length; i++) {
            list[i].selected = selectAllStatus; // 改变所有商品状态
        }
        if (selectAllStatus) {//全选
            num = list.length;
        } else {//反选
            num = 0;
        }
        console.log('num选中个数', num)
        //同步数据
        this.setData({
            selectAllStatus: selectAllStatus,
            list: list,
            num:num
        });
        //计算价格
        this.getTotalPrice();
    },
    //5.价格计算----------------------------------------------
    //思路：1.获取list容器  遍历容器 2.判断谁选中 sum+=price*num  3.渲染
    getTotalPrice(){
        const list=this.data.list;
        //遍历list 
        let sum=0;
        for(let i=0;i<list.length;i++){
            if(list[i].selected){
                sum+=list[i].price*list[i].num
            }
        }
        //同步
        this.setData({
            totalPrice:sum.toFixed(2)
        })
    },
    //6.修改购物车数量---增加 减少 
    //增加：1.点击增加按钮 触发事件--参数：当前元素  2.请求网络接口  3.最大限制 10 
    addNum(e){
        let index=e.currentTarget.dataset.index;
        let max=e.currentTarget.dataset.max;
        //获取购物车列表 list 
        const list=this.data.list;
        //获取之前的num数量  ++ 
        list[index].num++;
        if(list[index].num > max){
            list[index].num=max;
            wx.showToast({
              title: '最多数量'+max,
              icon:'none'
            })
            return;
        }
        //发送网络请求-----
        wx.request({
          url: host+updateUrl,
          data:{
              id:list[index].id,
              num:list[index].num
          },
          success:res=>{
              console.log(res.data);
              //成功后 修改数量
          }
        })
        this.setData({
            list:list
        })
        this.getTotalPrice()
    },
    //减少-----------------------------------------------
    reduceNum(e){
        let index=e.currentTarget.dataset.index;
        //获取购物车列表 list 
        const list=this.data.list;
        //获取之前的num数量  -- 
        list[index].num--;
        if(list[index].num < 1){
            list[index].num=1;
            wx.showToast({
              title: '数量最少为1',
              icon:'none'
            })
            return;
        }
        //发送网络请求-----
        wx.request({
          url: host+updateUrl,
          data:{
              id:list[index].id,
              num:list[index].num
          },
          success:res=>{
              console.log(res.data);
              //成功后 修改数量
          }
        })
        this.setData({
            list:list
        })
        this.getTotalPrice()
    },
    //7.删除购物车----------------------------------------
    //思路：1.点击删除按钮  --index当前行 2.删除当前的元素list里面的  3.网络请求-删数据
    delete(e){
        let index=e.currentTarget.dataset.index;
        const list=this.data.list;
        //删除数据库
        wx.request({
          url: host+deleteUrl,
          data:{
              id:list[index].id
          },
          success:res=>{
              console.log(res.data);
              //成功 删除视图数据 
                //加一个交互---弹框--是否删除这个商品：取消 确定 
                if(res.data.success){
                    //控制选中的状态 删除的时候 num-1 
                    if(list[index].selected){
                        this.data.num--;
                    }
                    //删除视图的list容器 数据 splice(当前的下标,数量)
                    list.splice(index--,1)               
                    //同步数据
                    this.setData({
                        list
                    })
                    //价格计算
                    this.getTotalPrice()
                    //问题：num 
                }
              //失败--
          }
        })

    },


    //1.手指触摸屏幕的时候触发事件-------------------------------
    touchStart(e){
        // console.log(e);
        //获取初始的手指的x坐标点
        this.setData({
            startX:e.touches[0].clientX
        })   
    },
    //2.手指离开的位置 x坐标点-----------------------
    touchEnd(e){
        // console.log('touchEnd',e);
        //获取当前滑动的元素 的下标 ==========
        const index=e.currentTarget.dataset.index;
        //获取list数组 
        const {list,startX}=this.data  
        //list[index].isActive=true/false 
        //获取结束的手指位置
        const endX=e.changedTouches[0].clientX;
        //获取手指的初始位置 和结束的位置 做比较 endx startX大小 判断手指左滑 右滑
        // const startX=this.data.startX;
        //判断左滑 删除出现
        if(startX-endX>5){
           list[index].isActive=true
        }else if(startX<endX-5){
            list[index].isActive=false
        }
        //同步数据list 
        this.setData({
            list:list
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    http(){
        wx.request({
            url: host+shopingUrl,
            success:res=>{
                console.log(res.data);
                if(res.data.status==200){
                    this.setData({
                        list:res.data.data.result.reverse()
                    })
                }
                
            }
          })
    },
    onLoad: function (options) {
      
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.http();
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})