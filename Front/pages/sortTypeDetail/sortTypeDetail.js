// pages/indexDetail/indexDetail.js
const {host,sortTypeDetail}=require('../../common/config.js')
Page({

    /**
     * 页面的初始数据
     */
    data: {
        info:'',//页面数据存储变量
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        console.log(options);//{}
        //获取从上一个页面跳转进来传递的url参数 ---
        //网络请求对应的接口数据---------------------------
        //交互动画 显示loading数据成功后 隐藏loading 
        wx.showLoading({
          title: '数据拼命加载中',
        })
        //导航栏loading 
        wx.showNavigationBarLoading()
        wx.request({
          url: host+sortTypeDetail,
          data:{
              id:options.mark
          },

          success:res=>{
              console.log(res.data);
              console.log(options.mark);
              wx.showToast({
                title: '加载完毕',
              })
              this.setData({
                  info:res.data[options.mark-1]
              })
          },
          
          complete(){
            wx.hideLoading()
            wx.hideNavigationBarLoading()
          }
        })
        
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})