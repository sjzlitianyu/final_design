// pages/shopDetail/shopDetail.js
const {host,bListDetail}=require('../../common/config')
Page({

    /**
     * 页面的初始数据
     */
    data: {
        info:''
    },
   //加入购物车数据---------------------------
   addShop(){
    //加购物车  一般参数id 
    const info=this.data.info
    wx.request({
      url: host+addShop,
      data:{
        name:info.name,//产品名称
        pic:info.pic,
        num:1,
        info:info.description,
        price:info.price
      },
      success:res=>{
        console.log(res.data);
        if(res.data.success){
            wx.showToast({
              title: '加入成功',
              icon:'none'
            })
        }
      } 
    })
},
    //进入购物车界面-----------------------
    goShoping(){
        wx.switchTab({
          url: '../shoping/shoping',
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        console.log(options);
        //请求对应的列表产品的数据
        wx.request({
          url: host+bListDetail,
          data:{
              id:options.mark
          },
          success:res=>{
              console.log(res.data);
              console.log(res.data[0]);
              this.setData({
                info:res.data[options.mark-1]
              })
          }
         
        })
      
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },
})