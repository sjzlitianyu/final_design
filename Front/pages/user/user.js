const app = getApp();
Page({
  data: {
    userinfo: '',
    nickname: '',
  },
  onShow() {
    console.log(111)
    var useinfo = wx.getStorageSync('userinfo')
    if (useinfo[0]) {
      console.log(useinfo[0])
      this.setData({
        userinfo: useinfo[0]
      })
    } else {
      this.setData({
        userinfo: useinfo
      })
    }
  },
  toLogin() {
    wx.navigateTo({
      url: '../login/login',
    })
  },
 //跳转到设置页面
 toSetup(){
  wx.openSetting({
    success (res) {
      console.log(res.authSetting)
      res.authSetting = {
        "scope.userInfo": true,
        "scope.userLocation": true
      }
    }
  })
    },

  toAdvice() {
    wx.navigateTo({
      url: '../advise/advise',
    })
  },
  toAdress() {
    wx.navigateTo({
      url: '../dairy/selectterm/selectterm',
    })
  },
  //跳转到收藏页面
  toCollect() {
    wx.navigateTo({
      url: '../collect/collect'
    })
  },
  exit(){
    wx.setStorageSync('userinfo',null);
    this.setData({
      userinfo:null
    })
  },

})