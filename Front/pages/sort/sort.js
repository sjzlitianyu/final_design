// pages/food/food.js
const fenlei=require('../../common/sortitem.js')
const {host,bookList}=require('../../common/config')
const app=getApp();
Page({
    data: {
        location:"衡水",
        num:1,//第一页数据
        itemArr:fenlei,
        listArr:[],//列表数据
        songArr:[],//音乐容器数据
        moreInfo:'',
    },
    /**
     * 生命周期函数--监听页面加载
     */
    http(){
      wx.request({
        url:host+bookList,
        data:{
          city:this.data.location,
          page:1
        },
        success:res=>{
            console.log(res.data);
            this.setData({
              listArr:res.data
            })
        }
       
      })

    },
    onLoad: function (options) {      
        //1.获取列表数据---------------------------
      this.http()
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {


      //方法3：本地存储
      const city=wx.getStorageSync('cityName')
      //有存储 再去请求数据
      if(city){
        this.setData({
              location:city
          })
        this.http()
      }
      
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
      //下拉刷新数据 ---重新请求数据--展示最新的数据--
      //下拉刷新数据 ---- 
      console.log('下拉刷新页面数据');
      //接口 下拉没有刷新更新数据的功能的，为了模拟效果-让数据更新
      //功能：1.下拉重新请求接口 2.请求上海的数据 
      wx.request({
        url: host+bookList,
        data:{
          city:'上海',
          page:1
        },
        success:res=>{
          console.log('更新数据',res.data);
          this.setData({
            listArr:res.data,
            num:1
          })
        }
      })
      
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
       //当用户上拉到底部 加载更多数据--------1.请求下一个数据 2.新请求数据+老数据
       this.data.num++;
       console.log('加载更多数据', this.data.num);
       //请求数据接口
       wx.request({
          url: host+bookList,
          data:{
            city:this.data.location,
            page:this.data.num
          },
          success:res=>{
            console.log(res.data);
            if(res.data.status==200){
              //把data里面的 数组数据+新请求的数组数据  arr1.concat(arr2)
              this.setData({
                listArr:this.data.listArr.concat(res.data.data),
                moreInfo:''
              })

            }else{
              console.log('没有更多数据');
              this.setData({
                moreInfo:'我是有底线的'
              })
            }
            
          }
       })


       
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})