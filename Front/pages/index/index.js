
const {host,bannerUrl,indexUrl,collect}=require('../../common/config.js')
const app=getApp()
console.log(app);

Page({
    /**
     * 页面的初始数据
     */
    data: {
        current:1,
        listArr:[],//列表数据
  
    },

    //点击列表 跳转页面
    goDetail(e){
        console.log(e.currentTarget.dataset.id)
        wx.request({
            url:'https://www.finaldesign.site:5050/essay/'+e.currentTarget.dataset.id,
            method:'GET',
            header: {
                "Content-Type": "application/x-www-form-urlencoded"
              },
            success:res=>{
                console.log(res.data[0])
                var title = res.data[0].essay_title;
                var txt = res.data[0].essay_txt;
                var img = res.data[0].essay_img;
                console.log(title)
                wx.navigateTo({
                  url: '../indexDetail/indexDetail',
                  success:function(res){
                      res.eventChannel.emit('accepttitle',{
                          data:title
                      })
                      res.eventChannel.emit('accepttxt',{
                          data:txt
                      })
                      res.eventChannel.emit('acceptimg',{
                          data:img
                      })
                  }
                })
            }
        })

    },
    swiperChange(e){//[0,1,2] 显示页面：1 2 3
        this.setData({
            current:e.detail.current+1
        })
    },  

    //生命周期函数--监听页面加载
    onLoad: function (options) {
      app.http(host+indexUrl,this,'listArr')
    }
      })