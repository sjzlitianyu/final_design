
const app = getApp()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    listArr:[],//列表数据
  },
  onShow() {
    wx.request({
      url: 'https://www.finaldesign.site:5050/collect',
      method: 'POST',
      data: {
        openid: app.globalData.openid,
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success:res=>{
        console.log(res.data)
        this.setData({
          listArr:res.data
        })

      }
    })
  },
  goDetail(e){
    wx.request({
        url:'https://www.finaldesign.site:5050/collect/'+e.currentTarget.dataset.id,
        method:'GET',
        header: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
        success:res=>{
            console.log(res.data[0])
            var title = res.data[0].title;
            var content = res.data[0].content;
            var img = res.data[0].img;
            console.log(title)
            wx.navigateTo({
              url: '../collectDetail/collectDetail',
              success:function(res){
                  res.eventChannel.emit('accepttitle',{
                      data:title
                  })
                  res.eventChannel.emit('acceptcontent',{
                      data:content
                  })
                  res.eventChannel.emit('acceptimg',{
                      data:img
                  })
              }
            })
        }
    })

},
})