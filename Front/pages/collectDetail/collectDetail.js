
// pages/indexDetail/indexDetail.js
const {
  host,
  indexDetail
} = require('../../common/config.js')
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    collectimg: '../../images/collect.png',
    infodata: [],
    info: '', //页面数据存储变量
  },

  onLoad: function (options) {
    var that = this;
    const eventChannel = this.getOpenerEventChannel()
    eventChannel.on('acceptcontent', data => {
      that.setData({
        infodata: data
      })
      console.log(app.globalData.openid)
      wx.request({
        url: 'https://www.finaldesign.site:5050/collect/select',
        method: 'POST',
        data: {
          openid: app.globalData.openid,
          content: this.data.infodata.data
        },
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: res => {
          console.log(res.data)
          if (res.data == 2) {
            that.setData({
              collectimg: "../../images/collected.png"
            })
          } else {
            that.setData({
              collectimg: "../../images/collect.png"
            })
          }
        }
      })
    })
    eventChannel.on('accepttitle', data => {
      that.setData({
        infotitle: data
      })
    })
    eventChannel.on('acceptimg', data => {
      that.setData({
        infoimg: data
      })
    })
  },




  collectIt(e) {
    var time = Date.parse(new Date()) / 1000;
    if (this.data.collectimg === "../../images/collect.png") {
      wx.request({
        url: 'https://www.finaldesign.site:5050/collect/insert',
        method: 'POST',
        data: {
          openid: app.globalData.openid,
          time: time,
          title: this.data.infotitle.data,
          img: this.data.infoimg.data,
          content: this.data.infodata.data,
        },
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: res => {
          this.setData({
            collectimg: "../../images/collected.png"
          })
          console.log('yes11');
          console.log(app.globalData.openid);
        }
      })
    } else {
      wx.request({
        url: 'https://www.finaldesign.site:5050/collect/delete',
        method: 'POST',
        data: {
          openid: app.globalData.openid,
          time: time,
          title: this.data.infotitle.data,
          img: this.data.infoimg.data,
          content: this.data.infodata.data,
        },
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
      })
      this.setData({
        collectimg: "../../images/collect.png"
      })
      console.log('yes22');
      console.log(app.globalData.openid);
    }
  },




})