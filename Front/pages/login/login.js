const app=getApp();
Page({
    data: {
    },
    getUserProfile(e){
        wx.getUserProfile({
          desc: '用于完善昵称和头像',
          success:(res)=>{
            if(res.userInfo){
              app.globalData.userInfo=res.userInfo;
              wx.setStorageSync('userinfo',res.userInfo)
              wx.request({
                url:'https://www.finaldesign.site:5050/login/openid',
                method: 'POST',
                data: {
                  openid: app.globalData.openid,
                  img:app.globalData.userInfo.avatarUrl,
                  name:app.globalData.userInfo.nickName
                },
                header: {
                  "Content-Type": "application/x-www-form-urlencoded"
                },
                success:res=>{
                  wx.switchTab({
                    url: '/pages/user/user',
                  })
                }
              })
            }
          }
        })
      },
})