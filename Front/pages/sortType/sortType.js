// pages/foodType/foodType.js
const {host,foodType}=require('../../common/config')
Page({

    /**
     * 页面的初始数据
     */
    data: {
        listArr:[]
    },
    goDetail(e){
      //跳转api 
      wx.navigateTo({
        url: '../sortTypeDetail/sortTypeDetail?mark='+e.currentTarget.dataset.id,
      })
console.log(e.currentTarget.dataset.id);
  },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        console.log(options);
        //请求数据---------------
        wx.showLoading({
          title: '数据加载中',
        })
        wx.request({
          url: host+foodType,
          data:{
              type:options.mark
          },
          header:{
            "Content-Type":"application/x-www-form-urlencoded"
          },
          method:'POST',
          dataType:'json',
          success:res=>{
              console.log(res.data);
              wx.showToast({
                title: '数据加载完毕',
              })
              this.setData({
                listArr:res.data
              })
          },
          complete(){
              wx.hideLoading()
          }
        })
        
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})