// pages/search/search.js
const {host,searchUrl}=require('../../common/config')
Page({

    /**
     * 页面的初始数据
     */
    data: {
        cityName:'',
        listArr:[]
    },
   
    getInputVal(e){
        // console.log(e.detail);
        //获取用户输入的内容后--查询数据-----------------
        if(!e.detail.value || e.detail.value ==' '){
            this.setData({
                listArr:''
              })
            return;
        }
        wx.request({
          url: host+searchUrl,
          data:{
            city:this.data.cityName,
            name:e.detail.value
          },
          header:{
            "Content-Type":"application/x-www-form-urlencoded"
          },
          method:'POST',
          dataType:'json',
          success:res=>{
              console.log(res.data);
              if(res.data.status == 200){
                  this.setData({
                    listArr:res.data.data
                  })
              }
          }
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        console.log(options);//{cityName:""}
        this.setData({
            cityName:options.cityName
        })
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})