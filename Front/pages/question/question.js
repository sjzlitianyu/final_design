// pages/question/question.js
const app = getApp();
Page({
  data: {
    type:''
  },
  clickStart:function () {
    console.log()
    this.setData({
      type:'start'
    });
    wx.navigateTo({
      url: `../answer_question/quesPage/quesPage?type=${this.data.type}`
    });
  },
  clickScience:function () {
    this.setData({
      type:'science'
    });
    wx.navigateTo({
      url: `../answer_question/quesPage/quesPage?type=${this.data.type}`
    });
  },
  clickMedicine:function () {
    this.setData({
      type:'medicine'
    });
    wx.navigateTo({
      url: `../answer_question/quesPage/quesPage?type=${this.data.type}`
    });
  }
})