// pages/selectCity/selectCity.js
const {host,locationUrl,hotCity}=require('../../common/config')
const api=getApp();//{globalData:{}}
Page({

    /**
     * 页面的初始数据
     */
    data: {
      cityList:[]
    },
    //1.点击定位按钮-------------------
    getLocation(){
        //获取小程序的经纬度  api --位置----
        wx.getLocation({
            success:res=>{
                console.log(res);
                //获取经纬度后  解析对应的城市位置------
                wx.request({
                  url: host+locationUrl,
                  data:{
                    longitude:res.longitude,
                    latitude:res.latitude
                  },
                  success:result=>{
                    //   console.log(result.data.result.address_component.city);
                      var city=result.data.result.address_component.city.slice(0,-1)
                      console.log(city);
                      //返回食疗坊的界面 带着你的城市----
                      //参数传递：1.url地址  2.appjs全局globalData  3.本地存储 
                      //1.路由-跳转页面  2.传递参数
                      //方法1：url参数传递：    switchTab不能url传递参数  -------
                       //wx.reLaunch({ url:'../food/food?city='+city }) 跳转 传递参数--food.js里面的onload接受
                      //缺点：重新渲染数据

                      //方法2：appjs全局globalData
                      // api.globalData.cityName=city;

                      //方法3：把数据存储到本地--永久存储----
                      wx.setStorageSync('cityName', city)
                                             
                      wx.switchTab({
                          url: '../food/food',
                      })


                  }
                })
            }
        })

    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
      //2.热门城市------------------------------
      wx.request({
        url: host+hotCity,
        success:res=>{
          console.log(res.data);
            this.setData({
              cityList:res.data.data
            })
        }
      })
    },
    //3.点击跳转到食疗坊界面---把点击的热门城市传递给食疗坊----------
    goFood(e){
      console.log(e.currentTarget.dataset.city);
      wx.setStorageSync('cityName', e.currentTarget.dataset.city)
      wx.switchTab({
        url: '../food/food',
      })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})