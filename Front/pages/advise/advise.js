
Page({
  onLoad: function (options) {
    // common.dataLoading("页面加载中","loading");
    var that = this;

  },
  onReady: function () {
  },
  onShow: function () {
    
  },
  onHide: function () {
  },
  onUnload: function (event) {
  },
  formSubmit: function (e) {//提交建议
    if (e.detail.value.advise == "" || e.detail.value.advise == null) {
      common.dataLoading("建议不能为空", "loading");
    }
    else {

      wx.request({
        // url: 'http://82.157.94.115:2005/advise',
        url: 'https://www.finaldesign.site:5050/help/insert',
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        data: {
         "advise":e.detail.value.advise
        },
        success: function (res) {
          console.log(e.detail.value.advise)
          if (res.statusCode === 200) {
            wx.showToast({
              title: '宝贵意见已收到',
            })
          } else {
            console.log('提交失败')
          }
        }
      })
    }
  },
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh()
  }
})
