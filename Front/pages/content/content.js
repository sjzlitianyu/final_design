const app=getApp();
Page({
    data: {
        onp:false,//执行旋转动画
        start:false,//显示答题界面
        //qdata:'',//问题库
        qdata:[{"question":'1+1=2',"answer":"对"},{"question":'1+2=2',"answer":"错"},{"question":'1+3=2',"answer":"对"},{"question":'1+4=2',"answer":"错"},{"question":'radio 标识。当该radio 选中时，radio-group 的 change 事件会携带radio的value',"answer":"对"},{"question":'1+6=2',"answer":"错"},{"question":'1+7=2',"answer":"对"},{"question":'1+8=2',"answer":"对"}],
        adata:'',//问题切割
        question:'',//当前问题
        numt:1,
        sta:'on',//回答正确错误显示图片
        code:0,//分数
        btn:true,//隐藏按钮
    },
    onLoad(){
        var that=this;
        wx.request({
            url:`${app.globalData.url}/question`,
            success(res){
                that.setData({qdata:res.data})
            }
        })
    },
    playit(){
       var that=this;
       var q=[];
       var k=Math.floor(Math.random()*that.data.qdata.length)
       that.setData({numt:1})
       for(var i=0;i<5;i++){
           var j=(k+i+1)%that.data.qdata.length;
           q=[...q,that.data.qdata[j]]
       }
       that.setData({adata:q});
       that.setData({question:that.data.adata[0].question})
        that.setData({onp:true})
        var idf=setTimeout(()=>{
            that.setData({start:true})
            that.setData({idf:idf})
            wx.vibrateLong()
        },3000)
        var ids=setTimeout(()=>{
            that.setData({onp:false})
            that.setData({ids:ids})
        },3500)
       
    },
   clo(idc){
        this.setData({btn:true})
        this.setData({code:0})
        this.setData({start:false})
        if(idc){
            clearTimeout(idc);
        }
   },
   wrgo(e){
       var that=this; 
       var infos=e.currentTarget.dataset.info;
       var i=that.data.numt;
       that.setData({btn:false})
       if(i<5){
            if(that.data.adata[i-1].answer===infos){
                that.setData({sta:'yes'});
                that.setData({code:parseInt(that.data.code)+20})
            }else{
                that.setData({sta:'no'});
            }
            setTimeout(()=>{
                that.setData({question:that.data.adata[i].question})
                that.setData({numt:that.data.numt+1})
                that.setData({btn:true})
                that.setData({sta:'on'});
            },1000)  
       }else{
            if(that.data.adata[i-1].answer===infos){
                that.setData({sta:'yes'});
                that.setData({code:20+that.data.code})
            }else{
                that.setData({sta:'no'});
            }
            setTimeout(()=>{
                that.setData({sta:'on'});
                this.setData({question:`本次测验的得分为：${that.data.code}`})
            },1000)
            var idc=setTimeout(()=>{
                this.clo(idc);
            },6000)
       }
   },
})