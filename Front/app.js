App({
  onLaunch: function () {
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
    wx.login({
      timeout: 1000,
      success: res => {
        wx.request({
          url: 'https://www.finaldesign.site:5050/login',
          data: {
            code: res.code,
          },
          method: 'POST',
          success: res => {
            this.globalData.openid = res.data.openid;
            wx.request({
              url: 'https://www.finaldesign.site:5050/login/openid',
              method: 'POST',
              data: {
                openid: res.data.openid,
                img:'',
                name:''
              },
              header: {
                "Content-Type": "application/x-www-form-urlencoded"
              },
              success:res=>{
                if(res.data=== "是个新用户，已记录到数据库中"){
                  this.globalData.login=false;
                  wx.switchTab({
                    url:'/pages/login/login',
                  })
                }else{
                  wx.setStorageSync('userinfo',res.data)    
                  // wx.switchTab({
                  //   url: '/pages/index/index',
                  // })
                }
              }
            })
          },
          fail: () => {  
            console.log('error');
          }
        })
      }
    })
  },
  globalData: {
    login:true,
    url: "https://www.finaldesign.site:5050/",
    userInfo: null,
    openid:'',
    postinfo: [],
    img: [],
    location:{name:'',address:''}
  },
http(url,that,arr){
  wx.request({
      url:url,
      success:res=>{
          console.log(res.data);//小程序请求的数据 res.data
          //获取接口数据  把数据存在data里面 去渲染视图
          that.setData({
             [arr]:res.data.data
          })
      }
    }) 
},

  globalData:{
    cityName:'',//城市名称 
    userInfo:'',//用户信息
  }
})
