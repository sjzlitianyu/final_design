//公共的接口地址 域名端口配置 
module.exports={
   host:'https://www.finaldesign.site:5050',
   indexUrl:'/essay',//首页文章接口
    indexDetail:'/essay',//id=1001 首页详情接口
    bookList:'/book',//食疗坊列表接口 ?city=北京&page=1
    searchUrl:'/search',
    foodType:`/fenlei`,//分类详情页面 ?type=0
    bListDetail:'/bookdetails',//产品详情  ?id=1
    sortTypeDetail:'/content',
    collect:'/collect',
    login:'/login',
    //searchUrl:'/api/foods/select',//搜索接口  ?name=产品名字city=城市名
    locationUrl:'/api/lbs/location',//定位成功 ?latitude=39.90&longitude=116.4
    hotCity:'/api/hot/city',//热门城市

    shopingUrl:'/api/cart/list',//购物车接口
    addShop:'/api/cart/add',//加入购物车接口
    updateUrl:'/api/cart/update',//修改购物车数量
    deleteUrl:'/api/cart/delete',//删除数据库
    
    
}