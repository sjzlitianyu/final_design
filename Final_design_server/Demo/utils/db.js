let mysql = require('mysql')

var pool = mysql.createPool({
    host:'localhost',
    port:3306,
    database:'Final_design',
    user:'root',
    password:'Final_design'
})

//对数据库进行增删改查操作的基础
const query = (sql,callback) => {
    pool.getConnection((err,connection) =>{
        connection.query(sql,(err,rows) => {
            callback(err,rows)
            connection.release()  //中断连接
        })
    })
}


exports.query = query;