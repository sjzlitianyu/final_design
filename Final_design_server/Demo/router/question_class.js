//专门用来存放关于答题的所有接口
const Router = require('koa-router'); //引入路由
const question = new Router();
const db = require('../utils/db');
//写对应的接口

question.get('/',async ctx=>{
    let data = await new Promise((reslove,reject) => {
        return db.query(`select * from question_class`,(err,data) => {
            if(err) console.log(err);
            reslove(data);
            })
        })
        ctx.body = data;
});


//删除题目    
question.post('/delete/:id',async ctx=>{
    const {id} = ctx.request.params
    let searchSql = `delete from question_class where id="${id}"`
    let result = await new Promise((resolve, reject) => {
        return db.query(searchSql, (err, data) => {
            if (err) throw err
            resolve(data)
        })
    })
    let result0 = await new Promise((reslove,reject) => {
        return db.query(`select * from question_class`,(err,data) => {
            if(err) console.log(err);
            reslove(data);
        })
    })
    //返回所有数据
    ctx.body = result0;
});

//插入题目
question.post('/insert', async ctx => {

    let searchSql0 = `select MAX(id) from question_class`;
    let idresult = await new Promise((resolve, reject) => {
        return db.query(searchSql0,(err,data)=>{
            if(err) throw err
            resolve(data[0])
        })
    }) 


    const data = ctx.request.body
    console.log(data);
    let searchSql = `insert into question_class (id,question,answer,option_A,option_B,option_C,option_D) values 
    ("${++idresult['MAX(id)']}","${data.question}","${data.answer}","${data.option_A}","${data.option_B}","${data.option_C}","${data.option_D}")`
    
    let result = await new Promise((resolve, reject) => {
        return db.query(searchSql, (err, data) => {
            if (err) throw err
            resolve(data)
        })
    })
    let result0 = await new Promise((reslove,reject) => {
        return db.query(`select * from question_class`,(err,data) => {
            if(err) console.log(err);
            reslove(data);
        })
    })
    //返回所有数据
    ctx.body = result0;
})


//插入分数
question.post('/insertmark', async ctx => {

    let searchSql0 = `select MAX(id) from mark_class`;
    let idresult = await new Promise((resolve, reject) => {
        return db.query(searchSql0,(err,data)=>{
            if(err) throw err
            resolve(data[0])
        })
    }) 


    const data = ctx.request.body
    console.log(data);
    let searchSql = `insert into mark_class (id,openid,time,mark) values 
    ("${++idresult['MAX(id)']}","${data.openid}","${data.time}","${data.mark}")`
    
    let result = await new Promise((resolve, reject) => {
        return db.query(searchSql, (err, data) => {
            if (err) throw err
            resolve(data)
        })
    })
    let result0 = await new Promise((reslove,reject) => {
        return db.query(`select * from mark_class`,(err,data) => {
            if(err) console.log(err);
            reslove(data);
        })
    })
    //返回所有数据
    ctx.body = result0;
})


module.exports = question;