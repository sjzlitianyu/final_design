//专门用来存放关于图片的所有接口
const Router = require('koa-router'); //引入路由
const img = new Router();
const db = require('../utils/db');

//写对应的图片接口

img.get('/',async ctx=>{
    let mydata = await new Promise((reslove,reject) => {
        return db.query(`select * from img_class`,(err,data) => {
            if(err) throw(err);
            data.map(val=>{
                val.img_url = `https://finaldesign.site:5050${val.img_url}`
            })
            reslove(data)
            })
        })
        ctx.body = mydata;
    })



module.exports = img;