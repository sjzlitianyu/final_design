//专门用来存放关于药材等详情的所有接口
const Router = require('koa-router'); //引入路由
const content = new Router();
const db = require('../utils/db');
//写对应的接口

content.get('/',async ctx=>{
    let data = await new Promise((reslove,reject) => {
        return db.query(`select * from content_class`,(err,data) => {
            if(err) console.log(err);
            reslove(data);
            })
        })
        ctx.body = data;
    })

//获取方剂信息
content.get('/prescript',async ctx=>{

    let data = await new Promise((reslove,reject) => {
        return db.query(`select * from content_class where type='方剂'`,(err,data) => {
            if(err) console.log(err);
            reslove(data);
            })
        })
        ctx.body = data;
    })
//删除方剂信息    
content.post('/prescript/delete/:id',async ctx=>{
    const {id} = ctx.request.params
    let searchSql = `delete from content_class where id="${id}" and type="方剂"`
    let result = await new Promise((resolve, reject) => {
        return db.query(searchSql, (err, data) => {
            if (err) throw err
            resolve(data)
        })
    })
    let result0 = await new Promise((reslove,reject) => {
        return db.query(`select * from content_class where type='方剂'`,(err,data) => {
            if(err) console.log(err);
            reslove(data);
        })
    })
    //返回所有数据
    ctx.body = result0;
})

//添加方剂信息
content.post('/prescript/insert', async ctx => {

    let searchSql0 = `select MAX(type_id) from content_class where type='方剂'`;
    let idresult = await new Promise((resolve, reject) => {
        return db.query(searchSql0,(err,data)=>{
            if(err) throw err
            resolve(data[0])
        })
    }) 

    const data = ctx.request.body
    console.log(data);
    let searchSql = `insert into content_class (name_detail,record_detail,effect_detail,introduction_detail,usage_detail,type,type_id,img) values ("${data.name_detail}","${data.record_detail}","${data.effect_detail}","${data.introduction_detail}","${data.usage_detail}","方剂","${++idresult['MAX(type_id)']}","${data.img}")`
    
    let result = await new Promise((resolve, reject) => {
        return db.query(searchSql, (err, data) => {
            if (err) throw err
            resolve(data)
        })
    })
    let result0 = await new Promise((reslove,reject) => {
        return db.query(`select * from content_class where type='方剂'`,(err,data) => {
            if(err) console.log(err);
            reslove(data);
        })
    })
    //返回所有数据
    ctx.body = result0;
})

/**----------------------------------------------------------------------------------------------------- */
//获取药膳信息
content.get('/medicatedfood',async ctx=>{
    let data = await new Promise((reslove,reject) => {
        return db.query(`select * from content_class where type='药膳'`,(err,data) => {
            if(err) console.log(err);
            reslove(data);
            })
        })
        ctx.body = data;
    })
//删除药膳信息    
content.post('/medicatedfood/delete/:id',async ctx=>{
    const {id} = ctx.request.params
    let searchSql = `delete from content_class where id="${id}" and type="药膳"`
    let result = await new Promise((resolve, reject) => {
        return db.query(searchSql, (err, data) => {
            if (err) throw err
            resolve(data)
        })
    })
    let result0 = await new Promise((reslove,reject) => {
        return db.query(`select * from content_class where type='药膳'`,(err,data) => {
            if(err) console.log(err);
            reslove(data);
        })
    })
    //返回所有数据
    ctx.body = result0;
})
//添加药膳信息
content.post('/medicatedfood/insert', async ctx => {

    let searchSql0 = `select MAX(type_id) from content_class where type='药膳'`;
    let idresult = await new Promise((resolve, reject) => {
        return db.query(searchSql0,(err,data)=>{
            if(err) throw err
            resolve(data[0])
        })
    }) 

    const data = ctx.request.body
    console.log(data);
    let searchSql = `insert into content_class (name_detail,record_detail,effect_detail,introduction_detail,usage_detail,type,type_id,img) values ("${data.name_detail}","${data.record_detail}","${data.effect_detail}","${data.introduction_detail}","${data.usage_detail}","药膳","${++idresult['MAX(type_id)']}","${data.img}")`
    
    let result = await new Promise((resolve, reject) => {
        return db.query(searchSql, (err, data) => {
            if (err) throw err
            resolve(data)
        })
    })
    let result0 = await new Promise((reslove,reject) => {
        return db.query(`select * from content_class where type='药膳'`,(err,data) => {
            if(err) console.log(err);
            reslove(data);
        })
    })
    //返回所有数据
    ctx.body = result0;
})

/**----------------------------------------------------------------------------------------------------- */
//获取中药信息
content.get('/medicine',async ctx=>{
    let data = await new Promise((reslove,reject) => {
        return db.query(`select * from content_class where type='中药'`,(err,data) => {
            if(err) console.log(err);
            reslove(data);
            })
        })
        ctx.body = data;
    })

//删除中药信息    
content.post('/medicine/delete/:id',async ctx=>{
    const {id} = ctx.request.params
    let searchSql = `delete from content_class where id="${id}" and type="中药"`
    let result = await new Promise((resolve, reject) => {
        return db.query(searchSql, (err, data) => {
            if (err) throw err
            resolve(data)
        })
    })
    let result0 = await new Promise((reslove,reject) => {
        return db.query(`select * from content_class where type='中药'`,(err,data) => {
            if(err) console.log(err);
            reslove(data);
        })
    })
    //返回所有数据
    ctx.body = result0;
})
//添加药膳信息
content.post('/medicine/insert', async ctx => {

    let searchSql0 = `select MAX(type_id) from content_class where type='中药'`;
    let idresult = await new Promise((resolve, reject) => {
        return db.query(searchSql0,(err,data)=>{
            if(err) throw err
            resolve(data[0])
        })
    }) 

    const data = ctx.request.body
    console.log(data);
    let searchSql = `insert into content_class (name_detail,record_detail,effect_detail,introduction_detail,usage_detail,type,type_id,img) values ("${data.name_detail}","${data.record_detail}","${data.effect_detail}","${data.introduction_detail}","${data.usage_detail}","中药","${++idresult['MAX(type_id)']}","${data.img}")`
    
    let result = await new Promise((resolve, reject) => {
        return db.query(searchSql, (err, data) => {
            if (err) throw err
            resolve(data)
        })
    })
    let result0 = await new Promise((reslove,reject) => {
        return db.query(`select * from content_class where type='中药'`,(err,data) => {
            if(err) console.log(err);
            reslove(data);
        })
    })
    //返回所有数据
    ctx.body = result0;
})


/**----------------------------------------------------------------------------------------------------- */
//获取穴位信息
content.get('/acupoint',async ctx=>{
    let data = await new Promise((reslove,reject) => {
        return db.query(`select * from content_class where type='穴位'`,(err,data) => {
            if(err) console.log(err);
            reslove(data);
            })
        })
        ctx.body = data;
    })

//删除穴位信息    
content.post('/acupoint/delete/:id',async ctx=>{
    const {id} = ctx.request.params
    let searchSql = `delete from content_class where id="${id}" and type="穴位"`
    let result = await new Promise((resolve, reject) => {
        return db.query(searchSql, (err, data) => {
            if (err) throw err
            resolve(data)
        })
    })
    let result0 = await new Promise((reslove,reject) => {
        return db.query(`select * from content_class where type='穴位'`,(err,data) => {
            if(err) console.log(err);
            reslove(data);
        })
    })
    //返回所有数据
    ctx.body = result0;
})
//添加穴位信息
content.post('/acupoint/insert', async ctx => {

    let searchSql0 = `select MAX(type_id) from content_class where type='穴位'`;
    let idresult = await new Promise((resolve, reject) => {
        return db.query(searchSql0,(err,data)=>{
            if(err) throw err
            resolve(data[0])
        })
    }) 

    const data = ctx.request.body
    console.log(data);
    let searchSql = `insert into content_class (name_detail,record_detail,effect_detail,introduction_detail,usage_detail,type,type_id,img) values ("${data.name_detail}","${data.record_detail}","${data.effect_detail}","${data.introduction_detail}","${data.usage_detail}","穴位","${++idresult['MAX(type_id)']}","${data.img}")`
    
    let result = await new Promise((resolve, reject) => {
        return db.query(searchSql, (err, data) => {
            if (err) throw err
            resolve(data)
        })
    })
    let result0 = await new Promise((reslove,reject) => {
        return db.query(`select * from content_class where type='穴位'`,(err,data) => {
            if(err) console.log(err);
            reslove(data);
        })
    })
    //返回所有数据
    ctx.body = result0;
})

module.exports = content;