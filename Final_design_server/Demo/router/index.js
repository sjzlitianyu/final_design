//router入口文件
const Router = require('koa-router');//引入路由
const router = new Router();
const home = require('./home');
const book = require('./book_class');
const essay = require('./essay_class');
const help = require('./help');
const img = require('./img_class');
const bookdetails = require('./bookdetails_class');
const fenlei = require('./fenlei_class');
const content = require('./content_class');
const search = require('./search');
const question = require('./question_class');
const login = require('./login');
const collect = require('./collect');
const journal = require('./journal_class');
const user = require('./user_class');
//后台单独
const dsmanage = require('./ds_admin_manage');


router.use('/home',home.routes(),home.allowedMethods());
router.use('/book',book.routes(),book.allowedMethods());
router.use('/essay',essay.routes(),essay.allowedMethods());
router.use('/help',help.routes(),help.allowedMethods());
router.use('/img',img.routes(),img.allowedMethods());
router.use('/bookdetails',bookdetails.routes(),bookdetails.allowedMethods());
router.use('/fenlei',fenlei.routes(),fenlei.allowedMethods());
router.use('/content',content.routes(),content.allowedMethods());
router.use('/search',search.routes(),search.allowedMethods());
router.use('/question',question.routes(),question.allowedMethods());
router.use('/login',login.routes(),login.allowedMethods());
router.use('/collect',collect.routes(),collect.allowedMethods());
router.use('/journal',journal.routes(),journal.allowedMethods());
router.use('/user',user.routes(),user.allowedMethods());
router.redirect('/','/home')
//后台单独
router.use('/dsmanage',dsmanage.routes(),dsmanage.allowedMethods());
//导出
module.exports = router