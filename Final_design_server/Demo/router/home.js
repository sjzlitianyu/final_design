//该文件专门存放关于首页所有接口

const Router = require('koa-router');
const home = new Router();
const db = require('../utils/db');

//写对应接口
home.get('/', async (ctx)=>{
    ctx.body = "首页"
})

home.get('/banner', async (ctx)=>{
    ctx.body = "首页-轮播图"
})

module.exports = home;