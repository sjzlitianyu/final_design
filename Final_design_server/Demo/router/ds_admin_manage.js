//这个文件专门来存放关于后台系统管理的所有接口

const Router = require('koa-router');
const dsmanage = new Router();
const bodyparser = require('koa-bodyparser');
const db = require('../utils/db');

dsmanage.use(bodyparser()) //调用这个中间件之后，就可以拿到前端post过来的数据

//获取
dsmanage.get('/', async ctx => {
    let data = await new Promise((reslove,reject) => {
        return db.query(`select * from administrator_class`,(err,data) => {
            if(err) console.log(err);
            reslove(data);
        })
    })
    ctx.body = data;
})



//更改时根据id获取数据
dsmanage.get('/:id',async ctx=>{
    const {id} = ctx.request.params;
    let searchSql = `select * from administrator_class where administrator_id="${id}"`
    let result = await new Promise((resolve, reject) => {
        return db.query(searchSql,(err,data)=>{
            if(err) throw err
            resolve(data)
        })
    })
    ctx.body = result;
})

//删除管理员根据id获取数据 
dsmanage.post('/delete/:id',async ctx=>{
    const {id} = ctx.request.params
    let searchSql = `delete from administrator_class where administrator_id="${id}"`
    let result = await new Promise((resolve, reject) => {
        return db.query(searchSql, (err, data) => {
            if (err) throw err
            resolve(data)
        })
    })
    let result0 = await new Promise((reslove,reject) => {
        return db.query(`select * from administrator_class`,(err,data) => {
            if(err) console.log(err);
            reslove(data);
        })
    })
    //返回所有数据
    ctx.body = result0;
})

//编辑管理员根据id获取数据
dsmanage.put('/:id',async ctx=>{
    const {id} = ctx.request.params;
    const data = ctx.request.body;
    let searchSql = `update administrator_class set administrator_name="${data.administrator_name}", 
    administrator_password="${data.administrator_password}" where administrator_id="${id}"`
    let result = await new Promise((resolve, reject) => {
        return db.query(searchSql,(err,data)=>{
            if(err) throw err
            resolve(data)
        })
    })
    ctx.body = result
})



//添加管理员
dsmanage.post('/insert', async ctx => {
    let searchSql0 = `select MAX(administrator_id) from administrator_class`;
    let idresult = await new Promise((resolve, reject) => {
        return db.query(searchSql0,(err,data)=>{
            if(err) throw err
            resolve(data[0])
        })
    }) 



    const data = ctx.request.body
    console.log(data);
    let searchSql = `insert into administrator_class (administrator_id,administrator_name,administrator_password,administrator_token,administrator_PowerLevel) values 
    ("${++idresult['MAX(administrator_id)']}","${data.administrator_name}","${data.administrator_password}","${data.administrator_token}","二级权限")`
    
    let result = await new Promise((resolve, reject) => {
        return db.query(searchSql, (err, data) => {
            if (err) throw err
            resolve(data)
        })
    })//插入新数据
    let result0 = await new Promise((reslove,reject) => {
        return db.query(`select * from administrator_class`,(err,data) => {
            if(err) console.log(err);
            reslove(data);
        })
    })
    //返回所有数据
    ctx.body = result0;
})

//登录验证
dsmanage.post('/login', async ctx=>{
    const data = ctx.request.body
    console.log(data);
    console.log(data.username);
    let searchSql = `select administrator_password, administrator_token from administrator_class where administrator_name = "${data.username}"`

    let result = await new Promise((resolve, reject) => {
        return db.query(searchSql, (err, data) => {
            if (err) throw err
            resolve(data)
        })
    })//获取查询结果
    if (result.length == 0){
        ctx.body = '用户不存在'
    }else{
        console.log(result)
        if(data.password == result[0].administrator_password){
            ctx.body={
                code:'success',
                token:result[0].administrator_token
            }
        }else{
            ctx.body='用户密码错误'
        }
    }
})


//后台右上角获取已登录账户用户名
dsmanage.post('/login/getname',async ctx=>{
    const data = ctx.request.rawBody;
    let searchSql = `select * from administrator_class where administrator_token = "${data}"`
    let result = await new Promise((resolve, reject) => {
        return db.query(searchSql, (err, data) => {
            if (err) throw err
            resolve(data)
        })
    })
    console.log(result[0].administrator_name);
    ctx.body = result[0].administrator_name;
    
})

module.exports = dsmanage;