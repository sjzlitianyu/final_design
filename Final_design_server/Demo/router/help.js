//专门用来存放关于反馈的所有接口
const Router = require('koa-router'); //引入路由
const help = new Router();
const db = require('../utils/db');
const bodyparser = require('koa-bodyparser');


help.use(bodyparser())

help.post('/insert',async ctx =>{
    const data = ctx.request.body;
    const insertSql = `insert into help_class (advise,state) values ("${data.advise}","未处理")`
    let result = await new Promise ((reslove,reject)=>{
        return db.query(insertSql,(err,data)=>{
            if(err) console.log(err);
            reslove(data)
        })
    })
    ctx.body = "保存成功";
})

help.get('/', async ctx => {
    let data = await new Promise((reslove,reject) => {
        return db.query(`select * from help_class`,(err,data) => {
            if(err) console.log(err);
            reslove(data);
        })
    })
    ctx.body = data;
})

//反馈状态修改
help.put('/treated/:id',async ctx=>{
    const {id} = ctx.request.params;
    const data = ctx.request.body;
    let searchSql = `update help_class set state="已处理" where id="${id}"`
    let result = await new Promise((resolve, reject) => {
        return db.query(searchSql,(err,data)=>{
            if(err) throw err
            resolve(data)
        })
    })
    ctx.body = result
})

help.put('/untreated/:id',async ctx=>{
    const {id} = ctx.request.params;
    const data = ctx.request.body;
    let searchSql = `update help_class set state="未处理" where id="${id}"`
    let result = await new Promise((resolve, reject) => {
        return db.query(searchSql,(err,data)=>{
            if(err) throw err
            resolve(data)
        })
    })
    ctx.body = result
})



module.exports = help;