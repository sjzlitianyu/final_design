const Koa = require('koa2');//构造函数
const cors = require('koa2-cors');//跨域
const app = new Koa();//声明一个实例
const port = 5050//端口号
const router = require('./router/index'); //引入router文件夹中的index入口文件
const static = require('koa-static');//静态资源请求中间件
const https = require('https');
const sslify = require('koa-sslify').default;//实施HTTPS中间件, 可对任何传入请求强制实施HTTPS连接
const path = require('path');
const fs = require('fs');
const bodyPaser = require('koa-bodyparser')  //中间件，更方便的处理POST请求，在koa中获取post请求的参数
const options = {
    key: fs.readFileSync('../ssl/finaldesign.site.key'),
    cert: fs.readFileSync('../ssl/finaldesign.site_bundle.pem'),
   }

app.use(sslify());
app.use(static(path.join(__dirname,'assets')))
app.use(cors());   //后端允许跨域访问
app.use(bodyPaser());
app.use(router.routes(),router.allowedMethods())

https.createServer(options, app.callback()).listen(port, (err) => {
    if (err) {
      console.log('服务启动出错', err);
    } else {
      console.log('guessWord-server运行在' + port + '端口');
    }	
  })


/*
app.use(async (cyx)=>{
    //返回数据给页面 ctx.response.body=""
    ctx.response.body = "hello,koa";
})//调用中间件
*/

/*
app.listen(port,()=>{
    console.log(`server is running at http://localhost:${port}`)
})
*/