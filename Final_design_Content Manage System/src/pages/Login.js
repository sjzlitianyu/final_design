import React from 'react'
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Checkbox, Form, Input,message } from 'antd';
import {setToken} from '../utils/auth';
import { loginApi } from '../services/auth';
import './Login.css';

const Login = (props) => {

  const onFinish = (values) => {
    console.log('Received values of form: ', values);
    // setToken(values.username)
    // props.history.push('/admin')
    loginApi({
      username:values.username,
      password:values.password
    })
    .then(res=>{
      if(res.code === 'success'){
        message.success('登陆成功');
        setToken(res.token);
        props.history.push('/admin/dashboard')
      }else{
        message.info(res)
      }
    })
    .catch(err=>{
      console.log(err);
    })
  };

  
  return (<div className='background'>
    <div className='logindiv'>
    <p>记录中医小程序后台管理系统</p>
    <Form
      name="normal_login"
      className="login-form"
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
    >
      <Form.Item
        name="username"
        rules={[
          {
            required: true,
            message: 'Please input your Username!',
          },
        ]}
      >
        <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
      </Form.Item>
      <Form.Item
        name="password"
        rules={[
          {
            required: true,
            message: 'Please input your Password!',
          },
        ]}
      >
        <Input
          prefix={<LockOutlined className="site-form-item-icon" />}
          type="password"
          placeholder="Password"
        />
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit" className="login-form-button">
          Log in
        </Button>
      </Form.Item>
    </Form>
  </div>
  </div>
 
    
  );
};

export default Login