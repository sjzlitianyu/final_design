import React,{useEffect,useState} from 'react'
import { Card,Table,Tooltip } from "antd";
import { userApi } from '../../../services/messages';


const User = (props) => {
  console.log(props);
  //定义局部状态
  const [dataSource,setDatasource] = useState([]);
  const [total,setTotal] = useState(0);
  const [time,setTime] = useState('')

  useEffect(()=>{
    userApi()
    .then(res=>{
      console.log(res);
      setDatasource(res)
    })
  },[])
  //组件初始化的时候执行
  const columns = [{
    title:'序号',
    key:'id',
    width:80,
    align:'center',
    dataIndex:'id'
  },{
    title:'用户昵称',
    dataIndex:'name',
    width:80,
  },{
    title:'用户openid',
    dataIndex:'openId',
    width:80,
  },{
    title:'图片',
    width:160,
    dataIndex:'img',
    align:'center',
    onCell: ()=>{
      return {
        style:{
          maxWidth: 100,
          overflow: 'hidden',
          whiteSpace: 'nowrap',
          textOverflow: 'ellipsis',
          cursor: 'pointer'
        }
      }
    },
    render: (text)=><Tooltip placement="topLeft" title={text}>{text}</Tooltip>
  },{
    title:'时间',
    width:160,
    dataIndex:'time',
    align:'center'
  },
]
  return (
    <Card title='反馈意见' >
      <Table 
        columns={columns} 
        bordered 
        dataSource={dataSource} 
        rowKey='id'
        pagination={{total,defaultPageSize:5}}
        />
    </Card>
  )
}

export default User