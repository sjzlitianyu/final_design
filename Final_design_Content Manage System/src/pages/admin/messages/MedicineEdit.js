import { Button,Card,Form,Input,message } from 'antd'
import React from 'react'
import {createOnemedicine} from '../../../services/messages'
import { useState,useEffect } from 'react'

const { TextArea } = Input;


const PrescriptEdit = (props) => {
  console.log(props);
  //props.match.params.id存在的话表示当前为修改页面，否则为新增页面


  const onFinish = (values) => {
    console.log('Success:', values);
    createOnemedicine(values)
        .then(res=>{
        props.history.push('/admin/messages/medicine')
        message.info('添加成功')
        })
        .catch(err=>{
        console.log(err)
        })

  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Card title='添加新药材'>
      <Form onFinish={onFinish} onFinishFailed={onFinishFailed}>
        <Form.Item
          name='name_detail' 
          label='药材名称'
          rules={[{required:true, message:'请输入药材名称'}]}
          >
          <Input placeholder='请输入药材名称' />
        </Form.Item>
        <Form.Item
          name='record_detail' 
          label=' 始  载  于'
          rules={[{required:true, message:'请输入药材出处'}]}
          >
          <TextArea placeholder='请输入药材出处' />
        </Form.Item>
        <Form.Item 
          name="effect_detail"
          label='药材功效'
          rules={[{required:true, message:'请输入药材功效'}]}
          >
          <Input placeholder='请输入药材功效'/>
        </Form.Item>
        <Form.Item 
          name="introduction_detail"
          label='药材简介'
          rules={[{required:true, message:'请输入药材简介'}]}
          >
          <Input placeholder='请输入药材简介'/>
        </Form.Item>
        <Form.Item 
          name="usage_detail"
          label='用法用量'
          rules={[{required:true, message:'请输入药材用法用量'}]}
          >
          <Input placeholder='请输入药材用法用量'/>
        </Form.Item>
        <Form.Item 
          name="img"
          label='药材图片'
          rules={[{required:true, message:'请输入图片链接'}]}
          >
          <Input placeholder='请输入图片链接'/>
        </Form.Item>

        <Form.Item>
          <Button htmlType='submit' type='primary' style={{marginLeft:"50%"}}>保存</Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

export default PrescriptEdit