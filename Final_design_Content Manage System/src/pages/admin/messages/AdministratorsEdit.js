import { Button,Card,Form,Input,message } from 'antd'
import React from 'react'
import {createAdministratorApi, getAdministratorById,modifyOneAdministrator} from '../../../services/messages'
import { useState,useEffect } from 'react'

const { TextArea } = Input;


const AdministratorsEdit = (props) => {
    console.log(props);
    //props.match.params.id存在的话表示当前为修改页面，否则为新增页面

    //初始化时执行，取一次数据
    const [form] = Form.useForm();
    useEffect(()=>{
        if(props.match.params.id){
            getAdministratorById(props.match.params.id)
            .then(res=>{
            console.log(res[0])
            form.setFieldsValue({
                administrator_name:res[0].administrator_name,
                administrator_password:res[0].administrator_password,
            })
            })
        }else{
            getAdministratorById(props.match.params.id)
            .then(res=>{
            console.log(res[0])
            form.setFieldsValue({
                administrator_name:res[0].administrator_name,
                administrator_password:res[0].administrator_password,
                administrator_token:res[0].administrator_token,
            })
            })
        }

    },[])



    const onFinish = (values) => {
        console.log('Success:', values);
        if(props.match.params.id){
            modifyOneAdministrator(props.match.params.id,values)
            .then(res=>{
                props.history.push('/admin/messages/administrators')
                message.info('修改成功')
            })
            .catch(err=>{
                console.log(err)
            })
        }else{
            createAdministratorApi(values)
            .then(res=>{
                props.history.push('/admin/messages/administrators')
                message.info('添加成功')
            })
            .catch(err=>{
                console.log(err)
            })
        }

    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
  return props.match.params.id?(
    <Card title='管理员编辑'>
      <Form onFinish={onFinish} onFinishFailed={onFinishFailed} form={form}>
        <Form.Item
          name='administrator_name' 
          label='用户名'
          rules={[{required:true, message:'请输入新用户名'}]}
          >
          <Input placeholder='请输入新用户名' />
        </Form.Item>
        <Form.Item
          name='administrator_password' 
          label='密码'
          rules={[{required:true, message:'请输入密码'}]}
          >
          <Input placeholder='请输入密码' />
        </Form.Item>

        <Form.Item>
          <Button htmlType='submit' type='primary' style={{marginLeft:"50%"}}>保存</Button>
        </Form.Item>
      </Form>
    </Card>
  ):(
    <Card title='管理员编辑'>
      <Form onFinish={onFinish} onFinishFailed={onFinishFailed} form={form}>
        <Form.Item
          name='administrator_name' 
          label='用户名'
          rules={[{required:true, message:'请输入新用户名'}]}
          >
          <Input placeholder='请输入新用户名' />
        </Form.Item>
        <Form.Item
          name='administrator_password' 
          label='密码'
          rules={[{required:true, message:'请输入密码'}]}
          >
          <Input placeholder='请输入密码' />
        </Form.Item>
        <Form.Item
          name='administrator_token' 
          label='token'
          rules={[{required:true, message:'请输入一串字符作为新的token'}]}
          >
          <Input placeholder='请输入一串字符作为新的token' />
        </Form.Item>

        <Form.Item>
          <Button htmlType='submit' type='primary' style={{marginLeft:"50%"}}>保存</Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

export default AdministratorsEdit