import React,{useEffect,useState} from 'react'
import { Button, Card,Table,Popconfirm,Tooltip, message } from "antd";
import { getAdministratorsApi,delOneAdministrator } from '../../../services/messages';

const Administrators = (props) => {
  console.log(props);
  //定义局部状态
  const [dataSource,setDatasource] = useState([]);
  const [total,setTotal] = useState(0);


  //判断是否为admin用户登录
  function isAdminLogined(){
    const loginState = localStorage.getItem('token')
    if (loginState=='asdadsadwdsadwasd'){
      return true;
    }else{
      return false;
    }
  };

  useEffect(()=>{
    getAdministratorsApi()
    .then(res=>{
      console.log(res);
      setDatasource(res)
    })
  },[])
  console.log(dataSource)
  //重新加载页面的方法
  const loadData =page=>{
    getAdministratorsApi(page).then(res => {
      setDatasource(res)
      setTotal(res.totalCount)
    })
  }
  //组件初始化的时候执行
  const admincolumns =[{
    title:'序号',
    key:'id',
    width:80,
    align:'center',
    dataIndex:'administrator_id'
   },{
    title:'管理员用户名',
    align:'center',
    dataIndex:'administrator_name'
   },{
    title:'管理员密码',
    align:'center',
    dataIndex:'administrator_password'
   },{
    title:'权限等级',
    align:'center',
    dataIndex:'administrator_PowerLevel'
   },{
      title:'操作',
      width:200,
      render:(txt,record,index)=>{
        return(<div>
          <Button  style={{margin:'0 1rem'}}type="primary"  size='small' onClick={()=>{
            props.history.push(`/admin/messages/administratorsEdit/${record.administrator_id}`)
          }}>更改</Button>
          <Popconfirm 
            title='是否需要移除此管理员？' 
            okText='Yes'
            onConfirm={()=>{delOneAdministrator(record.administrator_id).then(res=>{
              loadData(1);
            })
          }}
          >
            <Button  style={{margin:'0 1rem'}}type="primary"  size='small'>删除</Button>
          </Popconfirm>
          
        </div>)
      }
    }
  ]


  const columns = [{
    title:'序号',
    key:'id',
    width:80,
    align:'center',
    dataIndex:'administrator_id'
   },{
    title:'管理员用户名',
    align:'center',
    dataIndex:'administrator_name'
   },{
    title:'权限等级',
    align:'center',
    dataIndex:'administrator_PowerLevel'
   },]
  return isAdminLogined()?(
    <Card 
    title='管理员列表'
    extra={
      <Button type="primary" size='small' onClick={()=>props.history.push('/admin/messages/administratorsEdit')}>
        新增
      </Button>
    }>
      <Table 
        columns={admincolumns} 
        bordered 
        dataSource={dataSource} 
        rowKey='administrator_id'
        pagination={{total,defaultPageSize:5}}
        />
    </Card>
  ):(
    <Card title='管理员列表' >
      <Table 
        columns={columns} 
        bordered 
        dataSource={dataSource} 
        rowKey='administrator_id'
        pagination={{total,defaultPageSize:5}}
        />
    </Card>
  )
}

export default Administrators