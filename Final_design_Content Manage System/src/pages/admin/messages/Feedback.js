import React,{useEffect,useState} from 'react'
import { Button, Card,Table,Popconfirm,Tooltip, message } from "antd";
import { changeStateTreated, changeStateUnTreated, feedbackApi } from '../../../services/messages';

const Feedback = (props) => {
  console.log(props);
  //定义局部状态
  const [dataSource,setDatasource] = useState([]);
  const [total,setTotal] = useState(0);

  useEffect(()=>{
    feedbackApi()
    .then(res=>{
      console.log(res);
      setDatasource(res)
    })
  },[])
  console.log(dataSource)
  //重新加载页面的方法
  const loadData =page=>{
    feedbackApi(page).then(res => {
      setDatasource(res)
      setTotal(res.totalCount)
    })
  }
  //组件初始化的时候执行
  const columns = [{
    title:'序号',
    key:'id',
    width:80,
    align:'center',
    dataIndex:'id'
  },{
    title:'反馈内容',
    dataIndex:'advise',
    onCell: ()=>{
      return {
        style:{
          maxWidth: 100,
          overflow: 'hidden',
          whiteSpace: 'nowrap',
          textOverflow: 'ellipsis',
          cursor: 'pointer'
        }
      }
    },
    render: (text)=><Tooltip placement="topLeft" title={text}>{text}</Tooltip>
  },{
    title:'状态',
    width:160,
    dataIndex:'state',
    align:'center'
  },
  {
    title:'操作',
    width:160,
    render:(txt,record,index)=>{
      return(<div>
        <Popconfirm 
          title='此反馈是否已处理？' 
          onCancel={()=>(changeStateUnTreated(record.id).then(res=>{
            loadData();
            message.info('反馈未处理，请及时处理')
          }))} 
          onConfirm={()=>{changeStateTreated(record.id).then(res=>{
            loadData();
            message.info('反馈已处理')
          })}}
        >
          <Button  style={{margin:'0 1rem'}}type="primary"  size='small'>修改状态</Button>
        </Popconfirm>
        
      </div>)
    }
  }
]
  return (
    <Card title='反馈意见' >
      <Table 
        columns={columns} 
        bordered 
        dataSource={dataSource} 
        rowKey='id'
        pagination={{total,defaultPageSize:5}}
        />
    </Card>
  )
}

export default Feedback