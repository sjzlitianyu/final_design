import { Button,Card,Form,Input,message } from 'antd'
import React from 'react'
import {createOneQuestion} from '../../../services/messages'
import { useState,useEffect } from 'react'

const { TextArea } = Input;


const QuestionEdit = (props) => {
  console.log(props);
  //props.match.params.id存在的话表示当前为修改页面，否则为新增页面




  const onFinish = (values) => {
    console.log('Success:', values);
    createOneQuestion(values)
        .then(res=>{
        props.history.push('/admin/messages/question')
        message.info('添加成功')
        })
        .catch(err=>{
        console.log(err)
        })

  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Card title='题目编辑'>
      <Form onFinish={onFinish} onFinishFailed={onFinishFailed}>
        <Form.Item
          name='question' 
          label='题目题干'
          rules={[{required:true, message:'请输入题目题干'}]}
          >
          <Input placeholder='请输入题目题干' />
        </Form.Item>
        <Form.Item
          name='answer' 
          label='题目答案'
          rules={[{required:true, message:'请输入题目正确答案'}]}
          >
          <TextArea placeholder='请输入题目正确答案' />
        </Form.Item>
        <Form.Item 
          name="option_A"
          label='选项---A'
          rules={[{required:true, message:'请输入选项A'}]}
          >
          <Input placeholder='请输入选项A'/>
        </Form.Item>
        <Form.Item 
          name="option_B"
          label='选项---B'
          rules={[{required:true, message:'请输入选项B'}]}
          >
          <Input placeholder='请输入选项B'/>
        </Form.Item>
        <Form.Item 
          name="option_C"
          label='选项---C'
          rules={[{required:true, message:'请输入选项C'}]}
          >
          <Input placeholder='请输入选项C'/>
        </Form.Item>
        <Form.Item 
          name="option_D"
          label='选项---D'
          rules={[{required:true, message:'请输入选项D'}]}
          >
          <Input placeholder='请输入选项D'/>
        </Form.Item>

        <Form.Item>
          <Button htmlType='submit' type='primary' style={{marginLeft:"50%"}}>保存</Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

export default QuestionEdit