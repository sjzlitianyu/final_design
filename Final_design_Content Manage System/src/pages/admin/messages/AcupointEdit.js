import { Button,Card,Form,Input,message } from 'antd'
import React from 'react'
import {createOneacupoint} from '../../../services/messages'
import { useState,useEffect } from 'react'

const { TextArea } = Input;


const AcupointEdit = (props) => {
  console.log(props);
  //props.match.params.id存在的话表示当前为修改页面，否则为新增页面


  const onFinish = (values) => {
    console.log('Success:', values);
    createOneacupoint(values)
        .then(res=>{
        props.history.push('/admin/messages/acupoint')
        message.info('添加成功')
        })
        .catch(err=>{
        console.log(err)
        })

  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Card title='添加新药材'>
      <Form onFinish={onFinish} onFinishFailed={onFinishFailed}>
        <Form.Item
          name='name_detail' 
          label='穴位名称'
          rules={[{required:true, message:'请输入穴位名称'}]}
          >
          <Input placeholder='请输入穴位名称' />
        </Form.Item>
        <Form.Item
          name='record_detail' 
          label=' 始  载  于'
          rules={[{required:true, message:'请输入穴位出处'}]}
          >
          <TextArea placeholder='请输入穴位出处' />
        </Form.Item>
        <Form.Item 
          name="effect_detail"
          label='穴位主治'
          rules={[{required:true, message:'请输入穴位主治于'}]}
          >
          <Input placeholder='请输入穴位主治于'/>
        </Form.Item>
        <Form.Item 
          name="introduction_detail"
          label='穴位功效'
          rules={[{required:true, message:'请输入穴位功效'}]}
          >
          <Input placeholder='请输入穴位功效'/>
        </Form.Item>
        <Form.Item 
          name="usage_detail"
          label='经验应用'
          rules={[{required:true, message:'请输入穴位的经验应用'}]}
          >
          <Input placeholder='请输入穴位的经验应用'/>
        </Form.Item>
        <Form.Item 
          name="img"
          label='穴位图片'
          rules={[{required:true, message:'请输入图片链接'}]}
          >
          <Input placeholder='请输入图片链接'/>
        </Form.Item>

        <Form.Item>
          <Button htmlType='submit' type='primary' style={{marginLeft:"50%"}}>保存</Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

export default AcupointEdit