import React,{useEffect,useState} from 'react'
import { Button, Card,Table,Popconfirm,Tooltip, message } from "antd";
import {bookApi, delOneBook } from '../../../services/messages';

const Book = (props) => {
  console.log(props);
  //定义局部状态
  const [dataSource,setDatasource] = useState([]);
  const [total,setTotal] = useState(0);

  useEffect(()=>{
    bookApi()
    .then(res=>{
      console.log(res);
      setDatasource(res)
    })
  },[])
  console.log(dataSource)
  //重新加载页面的方法
  const loadData =page=>{
    bookApi(page).then(res => {
      setDatasource(res)
      setTotal(res.totalCount)
    })
  }
  //组件初始化的时候执行
  const columns = [{
    title:'序号',
    key:'id',
    width:80,
    align:'center',
    dataIndex:'id'
  },{
    title:'书籍名称',
    width:150,
    align:'center',
    dataIndex:'bookname'
  },{
    title:'内容简介',
    width:200,
    align:'center',
    dataIndex:'Content_introduction',
    onCell: ()=>{
      return {
        style:{
          maxWidth: 100,
          overflow: 'hidden',
          whiteSpace: 'nowrap',
          textOverflow: 'ellipsis',
          cursor: 'pointer'
        }
      }
    },
    render: (text)=><Tooltip placement="topLeft" title={text}>{text}</Tooltip>
  },{
    title:'书籍记载',
    width:80,
    align:'center',
    dataIndex:'Book_introduction',
    onCell: ()=>{
      return {
        style:{
          maxWidth: 100,
          overflow: 'hidden',
          whiteSpace: 'nowrap',
          textOverflow: 'ellipsis',
          cursor: 'pointer'
        }
      }
    },
    render: (text)=><Tooltip placement="topLeft" title={text}>{text}</Tooltip>
  },{
    title:'书籍访问地址',
    width:100,
    dataIndex:'Book_url',
    onCell: ()=>{
      return {
        style:{
          maxWidth: 100,
          overflow: 'hidden',
          whiteSpace: 'nowrap',
          textOverflow: 'ellipsis',
          cursor: 'pointer'
        }
      }
    },
    render: (text)=><Tooltip placement="topLeft" title={text}>{text}</Tooltip>
  },{
    title:'操作',
    width:50,
    render:(txt,record,index)=>{
      return(<div>
        <Popconfirm 
          title='确定删除此项吗？' 
          okText='Yes'
          onCancel={()=>message.info('取消删除')} 
          onConfirm={()=>{delOneBook(record.id).then(res=>{
            loadData(1);
            message.info('删除成功')
          })
        }}
        >
          <Button  style={{margin:'0 1rem'}}type="primary" danger size='small'>删除</Button>
        </Popconfirm>
        
      </div>)
    }
  }
]
  return (
    <Card 
      title='反馈意见'
      extra={
        <Button type="primary" size='small' onClick={()=>props.history.push('/admin/messages/bookedit')}>
          新增
        </Button>
      } >
      <Table 
        columns={columns} 
        bordered 
        dataSource={dataSource} 
        rowKey='id'
        pagination={{total,defaultPageSize:5}}
        />
    </Card>
  )
}

export default Book