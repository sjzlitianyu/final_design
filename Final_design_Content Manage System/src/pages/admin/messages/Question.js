import React,{useEffect,useState} from 'react'
import { Button, Card,Table,Popconfirm,Tooltip, message } from "antd";
import { delOneQuestion, questionApi } from '../../../services/messages';

const Question = (props) => {
  console.log(props);
  //定义局部状态
  const [dataSource,setDatasource] = useState([]);
  const [total,setTotal] = useState(0);

  useEffect(()=>{
    questionApi()
    .then(res=>{
      console.log(res);
      setDatasource(res)
    })
  },[])
  console.log(dataSource)
  //重新加载页面的方法
  const loadData =page=>{
    questionApi(page).then(res => {
      setDatasource(res)
      setTotal(res.totalCount)
    })
  }
  //组件初始化的时候执行
  const columns = [{
    title:'序号',
    key:'id',
    width:80,
    align:'center',
    dataIndex:'id'
  },{
    title:'题目题干',
    dataIndex:'question',
    onCell: ()=>{
      return {
        style:{
          maxWidth: 100,
          overflow: 'hidden',
          whiteSpace: 'nowrap',
          textOverflow: 'ellipsis',
          cursor: 'pointer'
        }
      }
    },
    render: (text)=><Tooltip placement="topLeft" title={text}>{text}</Tooltip>
  },{
    title:'正确答案',
    width:160,
    dataIndex:'answer',
    align:'center'
  },{
    title:'选项A',
    width:160,
    dataIndex:'option_A',
    align:'center'
  },{
    title:'选项B',
    width:160,
    dataIndex:'option_B',
    align:'center'
  },
  {
    title:'选项C',
    width:160,
    dataIndex:'option_C',
    align:'center'
  },
  {
    title:'选项D',
    width:160,
    dataIndex:'option_D',
    align:'center'
  },
  {
    title:'操作',
    width:160,
    render:(txt,record,index)=>{
      return(<div>
        <Popconfirm 
          title='确定删除此题？' 
          onCancel={()=>{
            loadData();
            message.info('取消删除')
          }}
          onConfirm={()=>{delOneQuestion(record.id).then(res=>{
            loadData();
            message.info('删除成功')
          })}}
        >
          <Button  style={{margin:'0 1rem'}}type="primary" danger size='small'>删除</Button>
        </Popconfirm>
        
      </div>)
    }
  }
]
  return (
    <Card 
      title='答题列表'
      extra={
        <Button type="primary" size='small' onClick={()=>props.history.push('/admin/messages/questionEdit')}>
          新增题目
        </Button>
      }>
      <Table 
        columns={columns} Question
        bordered 
        dataSource={dataSource} 
        rowKey='id'
        pagination={{total,defaultPageSize:5}}
        />
    </Card>
  )
}

export default Question