import { Button,Card,Form,Input,message } from 'antd'
import React from 'react'
import {createOneprescript} from '../../../services/messages'
import { useState,useEffect } from 'react'

const { TextArea } = Input;


const PrescriptEdit = (props) => {
  console.log(props);
  //props.match.params.id存在的话表示当前为修改页面，否则为新增页面


  const onFinish = (values) => {
    console.log('Success:', values);
    createOneprescript(values)
        .then(res=>{
        props.history.push('/admin/messages/prescript')
        message.info('添加成功')
        })
        .catch(err=>{
        console.log(err)
        })

  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Card title='添加新方剂'>
      <Form onFinish={onFinish} onFinishFailed={onFinishFailed}>
        <Form.Item
          name='name_detail' 
          label='方剂名称'
          rules={[{required:true, message:'请输入方剂名称'}]}
          >
          <Input placeholder='请输入方剂名称' />
        </Form.Item>
        <Form.Item
          name='record_detail' 
          label='方剂出处'
          rules={[{required:true, message:'请输入方剂出处'}]}
          >
          <TextArea placeholder='请输入方剂出处' />
        </Form.Item>
        <Form.Item 
          name="effect_detail"
          label='方剂功效'
          rules={[{required:true, message:'请输入方剂功效'}]}
          >
          <Input placeholder='请输入方剂功效'/>
        </Form.Item>
        <Form.Item 
          name="introduction_detail"
          label='方剂介绍'
          rules={[{required:true, message:'请输入方剂介绍'}]}
          >
          <Input placeholder='请输入方剂介绍'/>
        </Form.Item>
        <Form.Item 
          name="usage_detail"
          label='方剂主治'
          rules={[{required:true, message:'请输入方剂主治疾病'}]}
          >
          <Input placeholder='请输入方剂主治疾病'/>
        </Form.Item>
        <Form.Item 
          name="img"
          label='方剂图片'
          rules={[{required:true, message:'请输入图片链接'}]}
          >
          <Input placeholder='请输入图片链接'/>
        </Form.Item>

        <Form.Item>
          <Button htmlType='submit' type='primary' style={{marginLeft:"50%"}}>保存</Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

export default PrescriptEdit