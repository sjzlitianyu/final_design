import React,{useEffect,useState} from 'react'
import { Button, Card,Table,Popconfirm,Tooltip, message } from "antd";
import { delOnemedicine, acupointApi, delOneacupoint } from '../../../services/messages';

const Acupoint = (props) => {
  console.log(props);
  //定义局部状态
  const [dataSource,setDatasource] = useState([]);
  const [total,setTotal] = useState(0);

  useEffect(()=>{
    acupointApi()
    .then(res=>{
      console.log(res);
      setDatasource(res)
    })
  },[])
  console.log(dataSource)
  //重新加载页面的方法
  const loadData =page=>{
    acupointApi(page).then(res => {
      setDatasource(res)
      setTotal(res.totalCount)
    })
  }
  //组件初始化的时候执行
  const columns = [{
    title:'序号',
    key:'id',
    width:80,
    align:'center',
    dataIndex:'type_id'
  },{
    title:'穴位名称',
    width:100,
    align:'center',
    dataIndex:'name_detail'
  },{
    title:'出处',
    width:150,
    align:'center',
    dataIndex:'record_detail'
  },{
    title:'主治',
    align:'center',
    dataIndex:'effect_detail',
    onCell: ()=>{
      return {
        style:{
          maxWidth: 200,
          overflow: 'hidden',
          whiteSpace: 'nowrap',
          textOverflow: 'ellipsis',
          cursor: 'pointer'
        }
      }
    },
    render: (text)=><Tooltip placement="topLeft" title={text}>{text}</Tooltip>
  },{
    title:'功效',
    align:'center',
    dataIndex:'introduction_detail',
    onCell: ()=>{
      return {
        style:{
          maxWidth: 200,
          overflow: 'hidden',
          whiteSpace: 'nowrap',
          textOverflow: 'ellipsis',
          cursor: 'pointer'
        }
      }
    },
    render: (text)=><Tooltip placement="topLeft" title={text}>{text}</Tooltip>
  },{
    title:'经验应用',
    align:'center',
    dataIndex:'usage_detail',
    onCell: ()=>{
      return {
        style:{
          maxWidth: 200,
          overflow: 'hidden',
          whiteSpace: 'nowrap',
          textOverflow: 'ellipsis',
          cursor: 'pointer'
        }
      }
    },
    render: (text)=><Tooltip placement="topLeft" title={text}>{text}</Tooltip>
  },{
    title:'图片',
    width:80,
    align:'center',
    dataIndex:'img',
    onCell: ()=>{
      return {
        style:{
          maxWidth: 100,
          overflow: 'hidden',
          whiteSpace: 'nowrap',
          textOverflow: 'ellipsis',
          cursor: 'pointer'
        }
      }
    },
    render: (text)=><Tooltip placement="topLeft" title={text}>{text}</Tooltip>
  },{
    title:'操作',
    width:160,
    render:(txt,record,index)=>{
      return(<div>
        <Popconfirm 
          title='是否删除此项' 
          okText={'Yes'}
          onCancel={()=>(message.info('未删除'))} 
          onConfirm={()=>{delOneacupoint(record.id).then(res=>{
            loadData();
            message.info('已删除')
          })}}
        >
          <Button  style={{margin:'0 1rem'}}type="primary" danger size='small'>删除</Button>
        </Popconfirm>
        
      </div>)
    }
  }
]
  return (
    <Card 
      title='药材管理' 
      extra={
        <Button type="primary" size='small' onClick={()=>props.history.push('/admin/messages/acupointedit')}>
          新增
        </Button>
      }
    >
      <Table 
        columns={columns} 
        bordered 
        dataSource={dataSource} 
        rowKey='id'
        pagination={{total,defaultPageSize:5}}
        />
    </Card>
  )
}

export default Acupoint