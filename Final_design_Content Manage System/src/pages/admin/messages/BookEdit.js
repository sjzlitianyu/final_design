import { Button,Card,Form,Input,message } from 'antd'
import React from 'react'
import {createOneBook} from '../../../services/messages'
import { useState,useEffect } from 'react'

const { TextArea } = Input;


const BookEdit = (props) => {
  console.log(props);
  //props.match.params.id存在的话表示当前为修改页面，否则为新增页面

  //初始化时执行，取一次数据
  const [form] = Form.useForm();

  const onFinish = (values) => {
      console.log('Success:', values);
      createOneBook(values)
          .then(res=>{
            props.history.push('/admin/messages/book')
            message.info('添加成功')
          })
          .catch(err=>{
            console.log(err)
          })
  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Card title='书籍编辑'>
      <Form onFinish={onFinish} onFinishFailed={onFinishFailed} form={form}>
        <Form.Item
          name='bookname' 
          label='书籍名称'
          rules={[{required:true, message:'请输入书籍名称'}]}
          >
          <Input placeholder='请输入书籍名称' />
        </Form.Item>
        <Form.Item
          name='Content_introduction' 
          label='内容简介'
          rules={[{required:true, message:'请输入内容简介'}]}
          >
           <TextArea placeholder='请输入内容简介' style={{height:150}} />
        </Form.Item>
        <Form.Item 
          name="Book_introduction"
          label='书籍记载'
          rules={[{required:true, message:'请输入书籍记载'}]}
          >
          <TextArea placeholder='请输入书籍记载' style={{height:150}} />
        </Form.Item>
        <Form.Item
          name='Book_url' 
          label='书籍链接'
          rules={[{required:true, message:'请输入书籍链接'}]}
          >
          <Input placeholder='请输入书籍链接' />
        </Form.Item>

        <Form.Item>
          <Button htmlType='submit' type='primary' style={{marginLeft:"50%"}}>保存</Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

export default BookEdit