import { Button,Card,Form,Input,message } from 'antd'
import React from 'react'
import {createOnemedicatedfood} from '../../../services/messages'
import { useState,useEffect } from 'react'

const { TextArea } = Input;


const MedicatedFoodEdit = (props) => {
  console.log(props);
  //props.match.params.id存在的话表示当前为修改页面，否则为新增页面


  const onFinish = (values) => {
    console.log('Success:', values);
    createOnemedicatedfood(values)
        .then(res=>{
        props.history.push('/admin/messages/medicatedfood')
        message.info('添加成功')
        })
        .catch(err=>{
        console.log(err)
        })

  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Card title='添加新药膳'>
      <Form onFinish={onFinish} onFinishFailed={onFinishFailed}>
        <Form.Item
          name='name_detail' 
          label='药膳名称'
          rules={[{required:true, message:'请输入药膳名称'}]}
          >
          <Input placeholder='请输入药膳名称' />
        </Form.Item>
        <Form.Item
          name='record_detail' 
          label='药膳出处'
          rules={[{required:true, message:'请输入药膳出处'}]}
          >
          <TextArea placeholder='请输入药膳出处' />
        </Form.Item>
        <Form.Item 
          name="effect_detail"
          label='药膳食材'
          rules={[{required:true, message:'请输入药膳食材'}]}
          >
          <Input placeholder='请输入药膳食材'/>
        </Form.Item>
        <Form.Item 
          name="introduction_detail"
          label='药膳制法'
          rules={[{required:true, message:'请输入药膳制法'}]}
          >
          <Input placeholder='请输入药膳制法'/>
        </Form.Item>
        <Form.Item 
          name="usage_detail"
          label='药膳功效'
          rules={[{required:true, message:'请输入药膳功效'}]}
          >
          <Input placeholder='请输入药膳功效'/>
        </Form.Item>
        <Form.Item 
          name="img"
          label='药膳图片'
          rules={[{required:true, message:'请输入图片链接'}]}
          >
          <Input placeholder='请输入图片链接'/>
        </Form.Item>

        <Form.Item>
          <Button htmlType='submit' type='primary' style={{marginLeft:"50%"}}>保存</Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

export default MedicatedFoodEdit