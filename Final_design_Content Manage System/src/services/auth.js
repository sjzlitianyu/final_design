import {post} from '../utils/request'
/**
 * 用户登录
 * @param {*} user 
 * @returns 
 */
export function loginApi(user) {
    return post ('/dsmanage/login',user)
}

