import {get,post,put,del} from '../utils/request';

//文章页相关接口
/**
 * 获取列表
 */
export function essayApi(){
    return get('/essay');
}

/**
 * 创建数据
 * @param {*} data 
 */
export function createApi(data) {
    return post('/essay/insert',data)
}


/**
 * 根据id获取数据，用于修改界面
 * @param {*} id 
 * @returns 
 */
export function getOneById(id) {
    return get(`/essay/${id}`)
}


/**
 * 修改数据
 * @param {*} id 
 * @param {*} data 
 * @returns 
 */
export function modifyOne(id,data) {
    return put(`/essay/${id}`,data)
}

/**
 * 删除数据
 * @param {*} id 
 * @returns 
 */
export function delOne(id,data) {
    return post(`/essay/delete/${id}`,data)
}

/** ---------------------------------------------------分割线--------------------------------------- */

//反馈页相关接口

/**
 * 获取反馈意见
 */
export function feedbackApi(){
    return get(`/help`)
}

/**
 * 修改反馈处理状态
 * @param {*} id 
 * @returns 
 */
export function changeStateTreated(id){
    return put(`/help/treated/${id}`)
}
export function changeStateUnTreated(id){
    return put(`/help/untreated/${id}`)
}





/** ---------------------------------------------------分割线--------------------------------------- */

//管理员信息页相关接口
/**
 * 获取管理员信息
 */
export function getAdministratorsApi(){
    return get(`/dsmanage`)
}

/**
 * 添加一条管理员数据
 * @param {*} data 
 */
export function createAdministratorApi(data) {
    return post('/dsmanage/insert',data)
}


/**
 * 根据id获取管理员信息，用于修改界面
 * @param {*} id 
 * @returns 
 */
export function getAdministratorById(id) {
    return get(`/dsmanage/${id}`)
}

/**
 * 根据已登录的token值，获取该管理员用户名，显示在header中
 * @param {*} token 
 * @returns 
 */
export function getNameByToken(token){
    return post ('dsmanage/login/getname',token)
}

/**
 * 修改数据
 * @param {*} id 
 * @param {*} data 
 * @returns 
 */
export function modifyOneAdministrator(id,data) {
    return put(`/dsmanage/${id}`,data)
}

/**
 * 删除数据
 * @param {*} id 
 * @returns 
 */
export function delOneAdministrator(id,data) {
    return post(`/dsmanage/delete/${id}`,data)
}



/** ---------------------------------------------------分割线--------------------------------------- */
//题目管理界面相关接口
/**
 * 获得题目数据
 */
export function questionApi(){
    return get(`/question`)
}

/**
 * 根据id删除某一条题目数据
 * @param {*} id 
 * @returns 
 */
export function delOneQuestion(id){
    return post(`/question/delete/${id}`)
}

export function createOneQuestion(data){
    return post(`/question/insert`,data)
}

/** ---------------------------------------------------分割线--------------------------------------- */
//中医书籍管理页面相关接口
export function bookApi(){
    return get(`/bookdetails`)
}

export function delOneBook(id){
    return post(`/bookdetails/delete/${id}`)
}

export function createOneBook(data){
    return post(`/bookdetails/insert`,data)
}


/** ---------------------------------------------------分割线--------------------------------------- */
//用户收藏管理页面相关接口
export function collectApi(){
    return get(`/collect`)
}
/** ---------------------------------------------------分割线--------------------------------------- */
//用户管理
export function userApi(){
    return get(`/user`)
}



/** ---------------------------------------------------分割线--------------------------------------- */
/**
 * 获取方剂内容
 */
export function prescriptApi(){
    return get(`/content/prescript`)
}
//删除方剂内容
export function delOneprescript(id){
    return post(`/content/prescript/delete/${id}`)
}
//插入方剂内容
export function createOneprescript(data){
    return post(`/content/prescript/insert`,data)
}

/**
 * 获取药膳内容
 */
export function medicatedfoodApi(){
    return get(`/content/medicatedfood`)
}
//删除药膳内容
export function delOnemedicatedfood(id){
    return post(`/content/medicatedfood/delete/${id}`)
}
//插入药膳内容
export function createOnemedicatedfood(data){
    return post(`/content/medicatedfood/insert`,data)
}
/**
 * 获取中药内容
 */
export function medicineApi(){
    return get(`/content/medicine`)
}
//删除中药内容
export function delOnemedicine(id){
    return post(`/content/medicine/delete/${id}`)
}
//插入中药内容
export function createOnemedicine(data){
    return post(`/content/medicine/insert`,data)
}
/**
 * 获取穴位内容
 */
export function acupointApi(){
    return get(`/content/acupoint`)
}
//删除穴位内容
export function delOneacupoint(id){
    return post(`/content/acupoint/delete/${id}`)
}
//插入穴位内容
export function createOneacupoint(data){
    return post(`/content/acupoint/insert`,data)
}