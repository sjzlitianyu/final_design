import { Layout, Menu, Breadcrumb, Dropdown, Avatar,message } from 'antd';
import {Icon} from '@ant-design/compatible';
import {withRouter} from 'react-router-dom'
import Logo from './logo.png'
import { adminRoutes } from '../../routes';
import { clearToken } from '../../utils/auth';
import { getNameByToken } from '../../services/messages';
import { useState } from 'react';

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;

// 过滤一下，将需要显示的路由显示出来
const route = adminRoutes.filter(route=>route.isShow)


function Index(props) {
// 获取已登录管理员的用户名
  const token = localStorage.getItem('token')
  console.log(token)
  const [adminname,setAdminname] = useState();
  getNameByToken(token)
    .then(res=>{
      setAdminname(res)
    })

  //下拉菜单项
  const items = [
    {
      label: ('通知中心'),
      key: '0',
    },
    {
      label: ('设置'),
      key: '1',
    },
    {
      type: 'divider',
    },
    {
      label: ('退出登录'),
      key: '2',
    },
  ];
  //退出登录功能方法
  const onClick = ({ key }) => {
    if(key == '2'){
        message.info('退出登录');
        clearToken();
        props.history.push('./login')
    }else{
      message.info('该功能未完善')
    }
  
  };

  return (
    <Layout>
    <Header className="header" style={{background:'#E1D5A8'}}>
      <div className="logo" style={{height:'50px', width:'50px'}}>
        <img src = {Logo} alt='logo' style={{height:'60px', width:'60px', marginTop:'-5px'}}/>
      </div>
      <Dropdown menu={{items,onClick}}>
        <div style={{height:'50px',width:'200px',float:'right',marginTop:'-50px'}}>
          <Avatar>User</Avatar>
          <span style={{ marginLeft:'5px',color:'#6B8DFF'}}>管理员-{adminname}</span>
          <Icon type='down'/>
        </div>
      </Dropdown>
    </Header>
    <Layout>
      <Sider width={200} style={{ background: '#fff' }}>
        <Menu
          mode="inline"
          defaultSelectedKeys={['1']}
          defaultOpenKeys={['sub1']}
          style={{ height: '100%', borderRight: 0 }}
        >
          {route.map(route=>{
            return(
            <Menu.Item key={route.path} onClick={p=>props.history.push(p.key)}>
              <Icon type={route.icon} style={{marginRight:'10px'}} />
              {route.title}
            </Menu.Item>
            )
          })}
        </Menu>
      </Sider>
      <Layout style={{ padding: '0 24px 24px' }}>
        <Breadcrumb style={{ margin: '16px 0' }}>
          <Breadcrumb.Item>记录中医小程序后台管理系统</Breadcrumb.Item>
        </Breadcrumb>
        <Content
          style={{
            background: '#fff',
            padding: 24,
            margin: 0,
            minHeight: 280,
          }}
        >
          {props.children}
        </Content>
      </Layout>
    </Layout>
  </Layout>                      
  )
}

export default withRouter(Index)