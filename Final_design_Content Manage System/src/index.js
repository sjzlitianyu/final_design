import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import "antd/dist/reset.css";
import './index.css';
import App from './App';
import { mainRoutes } from './routes';
import reportWebVitals from './reportWebVitals';

const root = document.getElementById('root');
ReactDOM.render(
  <Router>
    <Switch>
      {/* 当访问admin时，走app组件。 */}
      <Route path="/admin" render={routeProps => <App {...routeProps}/>}/>
      {mainRoutes.map(route=>{
        return <Route key={route.path} path={route.path} component={route.component}/>
      })}
      <Redirect to='/admin/dashboard' from='/' />
      <Redirect to='/404'/>
    </Switch>
  </Router>,
  root
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
