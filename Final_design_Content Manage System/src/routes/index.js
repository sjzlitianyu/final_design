import Index from "../pages/admin/dashboard/Index"
import Administrators from "../pages/admin/messages/Administrators"
import Book from "../pages/admin/messages/Book"
import Collect from "../pages/admin/messages/Collect"
import Dailyrcord from "../pages/admin/messages/Dailyrcord"
import Essayedit from "../pages/admin/messages/Essayedit"
import Essay from "../pages/admin/messages/Essay"
import Medicine from "../pages/admin/messages/Medicine"
import Question from "../pages/admin/messages/Question"
import User from "../pages/admin/messages/User"
import Feedback from "../pages/admin/messages/Feedback"
import Login from "../pages/Login"
import PageNotFound from "../pages/PageNotFound"
import AdministratorsEdit from "../pages/admin/messages/AdministratorsEdit"
import QuestionEdit from "../pages/admin/messages/QuestionEdit"
import BookEdit from "../pages/admin/messages/BookEdit"
import Prescript from "../pages/admin/messages/Prescript"
import MedicatedFood from "../pages/admin/messages/MedicatedFood"
import MedicatedFoodEdit from "../pages/admin/messages/MedicatedFoodEdit"
import PrescriptEdit from "../pages/admin/messages/PrescriptEdit"
import MedicineEdit from "../pages/admin/messages/MedicineEdit"
import Acupoint from "../pages/admin/messages/Acupoint"
import AcupointEdit from "../pages/admin/messages/AcupointEdit"

export const mainRoutes = [{
    path:'/login',
    component:Login
},{
    path:'/404',
    component:PageNotFound
}]

export const adminRoutes = [{
    path:'/admin/dashboard',
    component:Index,
    isShow:true,
    title:'看板'
},{
    path:'/admin/messages',
    component:Essay,
    isShow:true,
    exact: true,
    icon:'container',
    title:'文章管理'
},
{
    path:'/admin/messages/book',
    component:Book,
    isShow:true,
    exact: true,
    icon:'book',
    title:'中医书籍管理'
},{
    path:'/admin/messages/prescript',
    component:Prescript,
    isShow:true,
    exact: true,
    icon:"form",
    title:'中医方剂管理'
},{
    path:'/admin/messages/medicine',
    component:Medicine,
    isShow:true,
    exact: true,
    icon:"coffee",
    title:'中医药材管理'
},{
    path:'/admin/messages/medicatedfood',
    component:MedicatedFood,
    isShow:true,
    exact: true,
    icon:"experiment",
    title:'中医药膳管理'
},{
    path:'/admin/messages/acupoint',
    component:Acupoint,
    isShow:true,
    exact: true,
    icon:"experiment",
    title:'中医穴位管理'
},{
    path:'/admin/messages/question',
    component:Question,
    isShow:true,
    exact: true,
    icon:'project',
    title:'测试题管理'
},{
    path:'/admin/messages/user',
    component:User,
    isShow:true,
    exact: true,
    icon:'user',
    title:'用户管理'
},{
    path:'/admin/messages/collect',
    component:Collect,
    isShow:true,
    exact: true,
    icon:"snippets",
    title:'用户收藏'
},{
    path:'/admin/messages/administrators',
    component:Administrators,
    isShow:true,
    exact: true,
    icon:'team',
    title:'管理员列表'
},{
    path:'/admin/messages/feedback',
    component:Feedback,
    isShow:true,
    exact: true,
    icon:'clock_circle',
    title:'反馈意见'
},{
    path:'/admin/messages/essayedit/:id?',
    component:Essayedit,
    isShow:false
},{
    path:'/admin/messages/administratorsedit/:id?',
    component:AdministratorsEdit,
    isShow:false 
},{
    path:'/admin/messages/questionedit/:id?',
    component:QuestionEdit,
    isShow:false 
},{
    path:'/admin/messages/bookedit/:id?',
    component:BookEdit,
    isShow:false 
},{
    path:'/admin/messages/prescriptedit/:id?',
    component:PrescriptEdit,
    isShow:false 
},{
    path:'/admin/messages/medicatedfoodedit/:id?',
    component:MedicatedFoodEdit,
    isShow:false 
},{
    path:'/admin/messages/medicineedit/:id?',
    component:MedicineEdit,
    isShow:false 
},{
    path:'/admin/messages/acupointedit/:id?',
    component:AcupointEdit,
    isShow:false 
}]